(window.webpackJsonp = window.webpackJsonp || []).push([
  [5],
  {
    20: function(e, t, n) {
      e.exports = n.p + "static/media/logo.1faf1147.png";
    },
    26: function(e, t, n) {
      "use strict";
      n.d(t, "b", function() {
        return l;
      }),
        n.d(t, "c", function() {
          return u;
        });
      var a = n(22),
        r = n(28),
        o = n.n(r),
        c = { isAuthenticated: !1, currentUser: {} };
      t.a = function() {
        var e =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : c,
          t = arguments.length > 1 ? arguments[1] : void 0;
        switch (t.type) {
          case "auth/AUTHENTICATE":
            return Object(a.a)({}, e, { isAuthenticated: t.authenticated });
          case "auth/SET_CURRENT_USER":
            return Object(a.a)({}, e, { currentUser: t.user });
          default:
            return e;
        }
      };
      var i = function(e) {
          return function(t) {
            return new Promise(function(n) {
              t({ type: "auth/SET_CURRENT_USER", user: e }),
                o.a.set("mywebsite", e),
                t({ type: "auth/AUTHENTICATE", authenticated: !0 }),
                n(e);
            });
          };
        },
        l = function() {
          return function(e) {
            return new Promise(function(t) {
              var n = o.a.getJSON("mywebsite");
              n ? (e(i(n)), t(n)) : t({});
            });
          };
        },
        u = function() {
          return function(e) {
            return new Promise(function(t) {
              e({ type: "auth/AUTHENTICATE", authenticated: !1 }),
                e({ type: "auth/SET_CURRENT_USER", user: {} }),
                o.a.remove("mywebsite"),
                t({});
            });
          };
        };
    },
    34: function(e, t, n) {
      "use strict";
      var a = n(52),
        r = n(9),
        o = n(10),
        c = n(12),
        i = n(11),
        l = n(13),
        u = n(0),
        s = n.n(u),
        m = n(31),
        d = n(51),
        p = n.n(d),
        h = n(20),
        f = n.n(h),
        b = (n(80), "https://cra-ssr.herokuapp.com"),
        E = "Costa Atl\xe1ntica MTB Tour",
        g = "".concat(b).concat(f.a),
        v = (function(e) {
          function t() {
            return (
              Object(r.a)(this, t),
              Object(c.a)(this, Object(i.a)(t).apply(this, arguments))
            );
          }
          return (
            Object(l.a)(t, e),
            Object(o.a)(t, [
              {
                key: "getMetaTags",
                value: function(e, t) {
                  var n = e.title,
                    a = e.description,
                    r = e.image,
                    o = e.contentType,
                    c = e.twitter,
                    i = e.noCrawl,
                    l = e.published,
                    u = e.updated,
                    s = e.category,
                    m = e.tags,
                    d = n ? (n + " | " + E).substring(0, 60) : E,
                    p = a
                      ? a.substring(0, 155)
                      : "This is a really awesome website where we can render on the server. Supa cool.",
                    h = r ? "".concat(b).concat(r) : g,
                    f = [
                      { itemprop: "name", content: d },
                      { itemprop: "description", content: p },
                      { itemprop: "image", content: h },
                      { name: "description", content: p },
                      { name: "twitter:card", content: "summary_large_image" },
                      { name: "twitter:site", content: "@cereallarceny" },
                      { name: "twitter:title", content: d },
                      { name: "twitter:description", content: p },
                      {
                        name: "twitter:creator",
                        content: c || "@cereallarceny"
                      },
                      { name: "twitter:image:src", content: h },
                      { property: "og:title", content: d },
                      { property: "og:type", content: o || "website" },
                      { property: "og:url", content: b + t },
                      { property: "og:image", content: h },
                      { property: "og:description", content: p },
                      { property: "og:site_name", content: E },
                      { property: "fb:app_id", content: "XXXXXXXXX" }
                    ];
                  return (
                    i &&
                      f.push({ name: "robots", content: "noindex, nofollow" }),
                    l && f.push({ name: "article:published_time", content: l }),
                    u && f.push({ name: "article:modified_time", content: u }),
                    s && f.push({ name: "article:section", content: s }),
                    m && f.push({ name: "article:tag", content: m }),
                    f
                  );
                }
              },
              {
                key: "render",
                value: function() {
                  var e = this.props,
                    t = e.children,
                    n = e.id,
                    r = e.className,
                    o = Object(a.a)(e, ["children", "id", "className"]);
                  return s.a.createElement(
                    "div",
                    { id: n, className: r },
                    s.a.createElement(p.a, {
                      htmlAttributes: {
                        lang: "en",
                        itemscope: void 0,
                        itemtype: "http://schema.org/".concat(
                          o.schema || "WebPage"
                        )
                      },
                      title: o.title ? o.title + " | " + E : E,
                      link: [
                        {
                          rel: "canonical",
                          href: b + this.props.location.pathname
                        }
                      ],
                      meta: this.getMetaTags(o, this.props.location.pathname)
                    }),
                    t
                  );
                }
              }
            ]),
            t
          );
        })(u.Component);
      t.a = Object(m.a)(v);
    },
    38: function(e, t, n) {
      "use strict";
      n.d(t, "b", function() {
        return u;
      }),
        n.d(t, "c", function() {
          return s;
        });
      var a = n(22),
        r = n(43),
        o = n.n(r),
        c = n(44),
        i = n.n(c),
        l = { currentProfile: {} };
      t.a = function() {
        var e =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : l,
          t = arguments.length > 1 ? arguments[1] : void 0;
        switch (t.type) {
          case "auth/SET_CURRENT_PROFILE":
            return Object(a.a)({}, e, { currentProfile: t.profile });
          default:
            return e;
        }
      };
      var u = function(e) {
          return function(t) {
            return new Promise(function(n) {
              setTimeout(function() {
                var a;
                (a =
                  1 === e
                    ? { id: e, name: "Pekka Rinne", image: o.a }
                    : { id: e, name: "Viktor Arvidsson", image: i.a }),
                  t({ type: "auth/SET_CURRENT_PROFILE", profile: a }),
                  n(a);
              }, 3e3);
            });
          };
        },
        s = function() {
          return function(e) {
            return new Promise(function(t) {
              e({ type: "auth/SET_CURRENT_PROFILE", profile: {} }), t({});
            });
          };
        };
    },
    43: function(e, t, n) {
      e.exports = n.p + "static/media/pekka.1eab475c.jpg";
    },
    44: function(e, t, n) {
      e.exports = n.p + "static/media/arvidsson.4d6f8e0d.jpg";
    },
    46: function(e, t, n) {
      e.exports = n.p + "static/media/x.e7fb8985.svg";
    },
    47: function(e, t, n) {
      e.exports = n.p + "static/media/menu.5cadddaa.svg";
    },
    48: function(e, t, n) {
      e.exports = n.p + "static/media/image1.b9902dfd.jpg";
    },
    49: function(e, t, n) {
      e.exports = n.p + "static/media/image2.99504bd4.jpg";
    },
    50: function(e, t, n) {
      e.exports = n.p + "static/media/image3.27a65ec1.jpg";
    },
    54: function(e, t, n) {
      e.exports = n(85);
    },
    73: function(e, t, n) {},
    74: function(e, t, n) {},
    75: function(e, t, n) {},
    83: function(e, t, n) {},
    84: function(e, t, n) {},
    85: function(e, t, n) {
      "use strict";
      n.r(t);
      var a = n(0),
        r = n.n(a),
        o = n(32),
        c = n(15),
        i = n(4),
        l = n.n(i),
        u = n(37),
        s = n(21),
        m = n(14),
        d = n(42),
        p = n(5),
        h = n(26),
        f = n(38),
        b = Object(m.c)({ auth: h.a, profile: f.a }),
        E = !(
          "undefined" !== typeof window &&
          window.document &&
          window.document.createElement
        ),
        g = n(9),
        v = n(10),
        w = n(12),
        j = n(11),
        y = n(13),
        O = n(31),
        N = (n(53), n(87)),
        T = n(20),
        x = n.n(T),
        _ = (n(73),
        [
          { to: "/etapas", text: "Etapas" },
          { to: "/categorias", text: "Categor\xedas" },
          { to: "/tarifas", text: "Tarifas" },
          { to: "/alojamiento", text: "Alojamiento" },
          { to: "/paquetes", text: "Paquetes" },
          { to: "/reglamento", text: "Reglamento" },
          { to: "/participantes", text: "Participantes" }
        ]),
        k = function(e, t) {
          return ("/" === e && t === e) || !("/" === e || !t.includes(e));
        },
        R = function(e) {
          var t = e.to,
            n = e.text,
            a = e.current;
          return r.a.createElement(
            "li",
            { className: k(t, a) ? "current nav-item" : "nav-item" },
            r.a.createElement(N.a, { to: t }, n)
          );
        },
        C = function(e) {
          var t = e.isAuthenticated,
            n = e.current;
          return r.a.createElement(
            "header",
            { id: "header", className: "header" },
            r.a.createElement(
              "div",
              { className: "logo-wrapper" },
              r.a.createElement(
                "a",
                { href: "/" },
                r.a.createElement("img", {
                  className: "logo",
                  src: x.a,
                  alt: "logo"
                })
              )
            ),
            r.a.createElement(
              "nav",
              { className: "nav" },
              _.map(function(e, a) {
                var o = r.a.createElement(
                  R,
                  Object.assign({ key: a, current: n }, e)
                );
                return e.hasOwnProperty("auth")
                  ? e.auth && t
                    ? o
                    : e.auth || t
                      ? null
                      : o
                  : o;
              }),
              r.a.createElement(
                "li",
                { className: "nav-item" },
                r.a.createElement(
                  "a",
                  {
                    target: "_blank",
                    href:
                      "https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/inscripcion_datos/"
                  },
                  "Inscripci\xf3n",
                  " "
                )
              )
            )
          );
        },
        A = n(16),
        P = (n(74), n(46)),
        M = n.n(P),
        S = (function(e) {
          function t() {
            return (
              Object(g.a)(this, t),
              Object(w.a)(this, Object(j.a)(t).apply(this, arguments))
            );
          }
          return (
            Object(y.a)(t, e),
            Object(v.a)(t, [
              {
                key: "render",
                value: function() {
                  var e = "hide";
                  return (
                    this.props.menuVisibility && (e = "show"),
                    r.a.createElement(
                      "div",
                      { id: "flyoutMenu", className: e },
                      r.a.createElement(
                        "button",
                        {
                          className: "btn",
                          onMouseDown: this.props.handleMouseDown
                        },
                        r.a.createElement("img", { src: M.a, alt: "icon" })
                      ),
                      r.a.createElement(
                        "ul",
                        null,
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/etapas" },
                            "Etapas"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/categorias" },
                            "Categor\xedas"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/tarifas" },
                            "Tarifas"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/alojamiento" },
                            "Alojamiento"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/paquetes" },
                            "Paquetes"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/reglamento" },
                            "Reglamento"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            N.a,
                            { className: "Header-link", to: "/participantes" },
                            "Participantes"
                          )
                        ),
                        r.a.createElement(
                          "h3",
                          null,
                          r.a.createElement(
                            "a",
                            {
                              className: "Header-link",
                              href:
                                "https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/inscripcion_datos/"
                            },
                            "Inscripci\xf3n"
                          )
                        )
                      )
                    )
                  );
                }
              }
            ]),
            t
          );
        })(a.Component),
        D = n(47),
        U = n.n(D),
        H = (function(e) {
          function t() {
            return (
              Object(g.a)(this, t),
              Object(w.a)(this, Object(j.a)(t).apply(this, arguments))
            );
          }
          return (
            Object(y.a)(t, e),
            Object(v.a)(t, [
              {
                key: "render",
                value: function() {
                  return r.a.createElement(
                    "button",
                    {
                      className: "btn btn-ham",
                      onMouseDown: this.props.handleMouseDown
                    },
                    r.a.createElement("img", {
                      className: "ham-mobile",
                      src: U.a,
                      alt: "icon"
                    })
                  );
                }
              }
            ]),
            t
          );
        })(r.a.Component),
        X = (function(e) {
          function t(e, n) {
            var a;
            return (
              Object(g.a)(this, t),
              ((a = Object(w.a)(
                this,
                Object(j.a)(t).call(this, e, n)
              )).state = { visible: !1 }),
              (a.handleMouseDown = a.handleMouseDown.bind(
                Object(A.a)(Object(A.a)(a))
              )),
              (a.toggleMenu = a.toggleMenu.bind(Object(A.a)(Object(A.a)(a)))),
              a
            );
          }
          return (
            Object(y.a)(t, e),
            Object(v.a)(t, [
              {
                key: "handleMouseDown",
                value: function(e) {
                  this.toggleMenu(), e.stopPropagation();
                }
              },
              {
                key: "toggleMenu",
                value: function() {
                  this.setState({ visible: !this.state.visible }),
                    console.log(this.state);
                }
              },
              {
                key: "render",
                value: function() {
                  return r.a.createElement(
                    "header",
                    { className: "header-mobile" },
                    r.a.createElement(
                      "div",
                      { className: "header-wrapper" },
                      r.a.createElement(
                        "div",
                        { className: "logo-wrapper" },
                        r.a.createElement(
                          "a",
                          { href: "/" },
                          r.a.createElement("img", {
                            className: "logo",
                            src: x.a,
                            alt: "logo"
                          })
                        )
                      ),
                      r.a.createElement(H, {
                        handleMouseDown: this.handleMouseDown
                      }),
                      r.a.createElement(S, {
                        handleMouseDown: this.handleMouseDown,
                        menuVisibility: this.state.visible
                      })
                    )
                  );
                }
              }
            ]),
            t
          );
        })(r.a.Component),
        I = (Object(c.connect)(function(e) {
          return { visible: e.visible };
        }, null)(X),
        n(75),
        n(48)),
        q = n.n(I),
        L = n(49),
        F = n.n(L),
        J = n(50),
        V = n.n(J),
        W = function() {
          return r.a.createElement(
            "footer",
            { id: "footer", className: "footer" },
            r.a.createElement(
              "div",
              { className: "row no-gutters" },
              r.a.createElement(
                "div",
                { className: "col-12 col-md-4" },
                r.a.createElement(
                  "div",
                  { className: "footer-image-wrapper" },
                  r.a.createElement("img", {
                    className: "img-fluid",
                    src: q.a,
                    alt: "image"
                  })
                )
              ),
              r.a.createElement(
                "div",
                { className: "col-12 col-md-4" },
                r.a.createElement(
                  "div",
                  { className: "footer-image-wrapper" },
                  r.a.createElement("img", {
                    className: "img-fluid",
                    src: F.a,
                    alt: "image"
                  })
                )
              ),
              r.a.createElement(
                "div",
                { className: "col-12 col-md-4" },
                r.a.createElement(
                  "div",
                  { className: "footer-image-wrapper" },
                  r.a.createElement("img", {
                    className: "img-fluid",
                    src: V.a,
                    alt: "image"
                  })
                )
              )
            ),
            r.a.createElement(
              "div",
              { className: "row no-gutters" },
              r.a.createElement(
                "div",
                { className: "col-12" },
                r.a.createElement(
                  "p",
                  { className: "copyright" },
                  "2019. GlobalDXT S.L."
                )
              )
            )
          );
        },
        B = n(88),
        G = n(89),
        z = n(34),
        K = function() {
          return r.a.createElement(
            z.a,
            {
              id: "not-found",
              title: "Not Found",
              description: "This is embarrassing.",
              noCrawl: !0
            },
            r.a.createElement("p", null, "Super embarrassing.")
          );
        },
        Q = l()({
          loader: function() {
            return Promise.all([n.e(9), n.e(2)]).then(n.bind(null, 149));
          },
          loading: function() {
            return null;
          },
          modules: ["homepage"]
        }),
        Y = l()({
          loader: function() {
            return n.e(0).then(n.bind(null, 137));
          },
          loading: function() {
            return null;
          },
          modules: ["etapas"]
        }),
        Z = l()({
          loader: function() {
            return n.e(1).then(n.bind(null, 138));
          },
          loading: function() {
            return null;
          },
          modules: ["tarifas"]
        }),
        $ = l()({
          loader: function() {
            return n.e(3).then(n.bind(null, 139));
          },
          loading: function() {
            return null;
          },
          modules: ["alojamiento"]
        }),
        ee = l()({
          loader: function() {
            return n.e(4).then(n.bind(null, 140));
          },
          loading: function() {
            return null;
          },
          modules: ["reglamento"]
        }),
        te = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 150));
          },
          loading: function() {
            return null;
          },
          modules: ["multimedia"]
        }),
        ne = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 141));
          },
          loading: function() {
            return null;
          },
          modules: ["categorias"]
        }),
        ae = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 142));
          },
          loading: function() {
            return null;
          },
          modules: ["participantes"]
        }),
        re = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 143));
          },
          loading: function() {
            return null;
          },
          modules: ["pruebas-global"]
        }),
        oe = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 144));
          },
          loading: function() {
            return null;
          },
          modules: ["pruebas-amigas"]
        }),
        ce = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 145));
          },
          loading: function() {
            return null;
          },
          modules: ["ropa-evento"]
        }),
        ie = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 146));
          },
          loading: function() {
            return null;
          },
          modules: ["embajadores"]
        }),
        le = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 147));
          },
          loading: function() {
            return null;
          },
          modules: ["contacto"]
        }),
        ue = l()({
          loader: function() {
            return n.e(6).then(n.bind(null, 148));
          },
          loading: function() {
            return null;
          },
          modules: ["paquetes"]
        }),
        se = function() {
          return r.a.createElement(
            B.a,
            null,
            r.a.createElement(G.a, { exact: !0, path: "/", component: Q }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/etapas",
              component: Y
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/tarifas",
              component: Z
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/alojamiento",
              component: $
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/reglamento",
              component: ee
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/categorias",
              component: ne
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/multimedia",
              component: te
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/participantes",
              component: ae
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/embajadores",
              component: ie
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/ropa-evento",
              component: ce
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/pruebas-global",
              component: re
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/pruebas-amigos",
              component: oe
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/paquetes",
              component: ue
            }),
            r.a.createElement(G.a, {
              exact: !0,
              path: "/contacto",
              component: le
            }),
            r.a.createElement(G.a, { component: K })
          );
        },
        me = (n(83),
        (function(e) {
          function t() {
            return (
              Object(g.a)(this, t),
              Object(w.a)(this, Object(j.a)(t).apply(this, arguments))
            );
          }
          return (
            Object(y.a)(t, e),
            Object(v.a)(t, [
              {
                key: "componentWillMount",
                value: function() {
                  E || this.props.establishCurrentUser();
                }
              },
              {
                key: "render",
                value: function() {
                  return r.a.createElement(
                    "div",
                    { id: "app" },
                    r.a.createElement(C, {
                      isAuthenticated: this.props.isAuthenticated,
                      current: this.props.location.pathname
                    }),
                    r.a.createElement(
                      "div",
                      { id: "content" },
                      r.a.createElement(se, null)
                    ),
                    r.a.createElement(W, null)
                  );
                }
              }
            ]),
            t
          );
        })(a.Component)),
        de = Object(O.a)(
          Object(c.connect)(
            function(e) {
              return { isAuthenticated: e.auth.isAuthenticated };
            },
            function(e) {
              return Object(m.b)({ establishCurrentUser: h.b }, e);
            }
          )(me)
        ),
        pe = (n(84),
        (function() {
          var e =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : "/",
            t = E ? Object(p.c)({ initialEntries: [e] }) : Object(p.a)(),
            n = [d.a, Object(s.routerMiddleware)(t)],
            a = m.d.apply(void 0, [m.a.apply(void 0, n)].concat([])),
            r = E ? {} : window.__PRELOADED_STATE__;
          return (
            E || delete window.__PRELOADED_STATE__,
            {
              store: Object(m.e)(Object(s.connectRouter)(t)(b), r, a),
              history: t
            }
          );
        })()),
        he = pe.store,
        fe = pe.history,
        be = r.a.createElement(
          c.Provider,
          { store: he },
          r.a.createElement(
            s.ConnectedRouter,
            { history: fe },
            r.a.createElement(
              u.Frontload,
              { noServerRender: !0 },
              r.a.createElement(de, null)
            )
          )
        ),
        Ee = document.querySelector("#root");
      !0 === Ee.hasChildNodes()
        ? l.a.preloadReady().then(function() {
            Object(o.hydrate)(be, Ee);
          })
        : Object(o.render)(be, Ee);
    }
  },
  [[54, 7, 8]]
]);
//# sourceMappingURL=main.a98dfdf3.chunk.js.map
