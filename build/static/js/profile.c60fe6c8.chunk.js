(window.webpackJsonp = window.webpackJsonp || []).push([
  [6],
  {
    130: function(e, a, t) {
      e.exports = t(131);
    },
    131: function(e, a, t) {
      var r =
          (function() {
            return this || ("object" === typeof self && self);
          })() || Function("return this")(),
        n =
          r.regeneratorRuntime &&
          Object.getOwnPropertyNames(r).indexOf("regeneratorRuntime") >= 0,
        l = n && r.regeneratorRuntime;
      if (((r.regeneratorRuntime = void 0), (e.exports = t(132)), n))
        r.regeneratorRuntime = l;
      else
        try {
          delete r.regeneratorRuntime;
        } catch (o) {
          r.regeneratorRuntime = void 0;
        }
    },
    132: function(e, a) {
      !(function(a) {
        "use strict";
        var t,
          r = Object.prototype,
          n = r.hasOwnProperty,
          l = "function" === typeof Symbol ? Symbol : {},
          o = l.iterator || "@@iterator",
          c = l.asyncIterator || "@@asyncIterator",
          i = l.toStringTag || "@@toStringTag",
          s = "object" === typeof e,
          d = a.regeneratorRuntime;
        if (d) s && (e.exports = d);
        else {
          (d = a.regeneratorRuntime = s ? e.exports : {}).wrap = y;
          var u = "suspendedStart",
            m = "suspendedYield",
            p = "executing",
            h = "completed",
            b = {},
            g = {};
          g[o] = function() {
            return this;
          };
          var f = Object.getPrototypeOf,
            E = f && f(f(P([])));
          E && E !== r && n.call(E, o) && (g = E);
          var v = (j.prototype = w.prototype = Object.create(g));
          (x.prototype = v.constructor = j),
            (j.constructor = x),
            (j[i] = x.displayName = "GeneratorFunction"),
            (d.isGeneratorFunction = function(e) {
              var a = "function" === typeof e && e.constructor;
              return (
                !!a &&
                (a === x || "GeneratorFunction" === (a.displayName || a.name))
              );
            }),
            (d.mark = function(e) {
              return (
                Object.setPrototypeOf
                  ? Object.setPrototypeOf(e, j)
                  : ((e.__proto__ = j), i in e || (e[i] = "GeneratorFunction")),
                (e.prototype = Object.create(v)),
                e
              );
            }),
            (d.awrap = function(e) {
              return { __await: e };
            }),
            q(O.prototype),
            (O.prototype[c] = function() {
              return this;
            }),
            (d.AsyncIterator = O),
            (d.async = function(e, a, t, r) {
              var n = new O(y(e, a, t, r));
              return d.isGeneratorFunction(a)
                ? n
                : n.next().then(function(e) {
                    return e.done ? e.value : n.next();
                  });
            }),
            q(v),
            (v[i] = "Generator"),
            (v[o] = function() {
              return this;
            }),
            (v.toString = function() {
              return "[object Generator]";
            }),
            (d.keys = function(e) {
              var a = [];
              for (var t in e) a.push(t);
              return (
                a.reverse(),
                function t() {
                  for (; a.length; ) {
                    var r = a.pop();
                    if (r in e) return (t.value = r), (t.done = !1), t;
                  }
                  return (t.done = !0), t;
                }
              );
            }),
            (d.values = P),
            (k.prototype = {
              constructor: k,
              reset: function(e) {
                if (
                  ((this.prev = 0),
                  (this.next = 0),
                  (this.sent = this._sent = t),
                  (this.done = !1),
                  (this.delegate = null),
                  (this.method = "next"),
                  (this.arg = t),
                  this.tryEntries.forEach(C),
                  !e)
                )
                  for (var a in this)
                    "t" === a.charAt(0) &&
                      n.call(this, a) &&
                      !isNaN(+a.slice(1)) &&
                      (this[a] = t);
              },
              stop: function() {
                this.done = !0;
                var e = this.tryEntries[0].completion;
                if ("throw" === e.type) throw e.arg;
                return this.rval;
              },
              dispatchException: function(e) {
                if (this.done) throw e;
                var a = this;
                function r(r, n) {
                  return (
                    (c.type = "throw"),
                    (c.arg = e),
                    (a.next = r),
                    n && ((a.method = "next"), (a.arg = t)),
                    !!n
                  );
                }
                for (var l = this.tryEntries.length - 1; l >= 0; --l) {
                  var o = this.tryEntries[l],
                    c = o.completion;
                  if ("root" === o.tryLoc) return r("end");
                  if (o.tryLoc <= this.prev) {
                    var i = n.call(o, "catchLoc"),
                      s = n.call(o, "finallyLoc");
                    if (i && s) {
                      if (this.prev < o.catchLoc) return r(o.catchLoc, !0);
                      if (this.prev < o.finallyLoc) return r(o.finallyLoc);
                    } else if (i) {
                      if (this.prev < o.catchLoc) return r(o.catchLoc, !0);
                    } else {
                      if (!s)
                        throw new Error(
                          "try statement without catch or finally"
                        );
                      if (this.prev < o.finallyLoc) return r(o.finallyLoc);
                    }
                  }
                }
              },
              abrupt: function(e, a) {
                for (var t = this.tryEntries.length - 1; t >= 0; --t) {
                  var r = this.tryEntries[t];
                  if (
                    r.tryLoc <= this.prev &&
                    n.call(r, "finallyLoc") &&
                    this.prev < r.finallyLoc
                  ) {
                    var l = r;
                    break;
                  }
                }
                l &&
                  ("break" === e || "continue" === e) &&
                  l.tryLoc <= a &&
                  a <= l.finallyLoc &&
                  (l = null);
                var o = l ? l.completion : {};
                return (
                  (o.type = e),
                  (o.arg = a),
                  l
                    ? ((this.method = "next"), (this.next = l.finallyLoc), b)
                    : this.complete(o)
                );
              },
              complete: function(e, a) {
                if ("throw" === e.type) throw e.arg;
                return (
                  "break" === e.type || "continue" === e.type
                    ? (this.next = e.arg)
                    : "return" === e.type
                      ? ((this.rval = this.arg = e.arg),
                        (this.method = "return"),
                        (this.next = "end"))
                      : "normal" === e.type && a && (this.next = a),
                  b
                );
              },
              finish: function(e) {
                for (var a = this.tryEntries.length - 1; a >= 0; --a) {
                  var t = this.tryEntries[a];
                  if (t.finallyLoc === e)
                    return this.complete(t.completion, t.afterLoc), C(t), b;
                }
              },
              catch: function(e) {
                for (var a = this.tryEntries.length - 1; a >= 0; --a) {
                  var t = this.tryEntries[a];
                  if (t.tryLoc === e) {
                    var r = t.completion;
                    if ("throw" === r.type) {
                      var n = r.arg;
                      C(t);
                    }
                    return n;
                  }
                }
                throw new Error("illegal catch attempt");
              },
              delegateYield: function(e, a, r) {
                return (
                  (this.delegate = {
                    iterator: P(e),
                    resultName: a,
                    nextLoc: r
                  }),
                  "next" === this.method && (this.arg = t),
                  b
                );
              }
            });
        }
        function y(e, a, t, r) {
          var n = a && a.prototype instanceof w ? a : w,
            l = Object.create(n.prototype),
            o = new k(r || []);
          return (
            (l._invoke = (function(e, a, t) {
              var r = u;
              return function(n, l) {
                if (r === p) throw new Error("Generator is already running");
                if (r === h) {
                  if ("throw" === n) throw l;
                  return U();
                }
                for (t.method = n, t.arg = l; ; ) {
                  var o = t.delegate;
                  if (o) {
                    var c = L(o, t);
                    if (c) {
                      if (c === b) continue;
                      return c;
                    }
                  }
                  if ("next" === t.method) t.sent = t._sent = t.arg;
                  else if ("throw" === t.method) {
                    if (r === u) throw ((r = h), t.arg);
                    t.dispatchException(t.arg);
                  } else "return" === t.method && t.abrupt("return", t.arg);
                  r = p;
                  var i = N(e, a, t);
                  if ("normal" === i.type) {
                    if (((r = t.done ? h : m), i.arg === b)) continue;
                    return { value: i.arg, done: t.done };
                  }
                  "throw" === i.type &&
                    ((r = h), (t.method = "throw"), (t.arg = i.arg));
                }
              };
            })(e, t, o)),
            l
          );
        }
        function N(e, a, t) {
          try {
            return { type: "normal", arg: e.call(a, t) };
          } catch (r) {
            return { type: "throw", arg: r };
          }
        }
        function w() {}
        function x() {}
        function j() {}
        function q(e) {
          ["next", "throw", "return"].forEach(function(a) {
            e[a] = function(e) {
              return this._invoke(a, e);
            };
          });
        }
        function O(e) {
          var a;
          this._invoke = function(t, r) {
            function l() {
              return new Promise(function(a, l) {
                !(function a(t, r, l, o) {
                  var c = N(e[t], e, r);
                  if ("throw" !== c.type) {
                    var i = c.arg,
                      s = i.value;
                    return s && "object" === typeof s && n.call(s, "__await")
                      ? Promise.resolve(s.__await).then(
                          function(e) {
                            a("next", e, l, o);
                          },
                          function(e) {
                            a("throw", e, l, o);
                          }
                        )
                      : Promise.resolve(s).then(
                          function(e) {
                            (i.value = e), l(i);
                          },
                          function(e) {
                            return a("throw", e, l, o);
                          }
                        );
                  }
                  o(c.arg);
                })(t, r, a, l);
              });
            }
            return (a = a ? a.then(l, l) : l());
          };
        }
        function L(e, a) {
          var r = e.iterator[a.method];
          if (r === t) {
            if (((a.delegate = null), "throw" === a.method)) {
              if (
                e.iterator.return &&
                ((a.method = "return"),
                (a.arg = t),
                L(e, a),
                "throw" === a.method)
              )
                return b;
              (a.method = "throw"),
                (a.arg = new TypeError(
                  "The iterator does not provide a 'throw' method"
                ));
            }
            return b;
          }
          var n = N(r, e.iterator, a.arg);
          if ("throw" === n.type)
            return (
              (a.method = "throw"), (a.arg = n.arg), (a.delegate = null), b
            );
          var l = n.arg;
          return l
            ? l.done
              ? ((a[e.resultName] = l.value),
                (a.next = e.nextLoc),
                "return" !== a.method && ((a.method = "next"), (a.arg = t)),
                (a.delegate = null),
                b)
              : l
            : ((a.method = "throw"),
              (a.arg = new TypeError("iterator result is not an object")),
              (a.delegate = null),
              b);
        }
        function S(e) {
          var a = { tryLoc: e[0] };
          1 in e && (a.catchLoc = e[1]),
            2 in e && ((a.finallyLoc = e[2]), (a.afterLoc = e[3])),
            this.tryEntries.push(a);
        }
        function C(e) {
          var a = e.completion || {};
          (a.type = "normal"), delete a.arg, (e.completion = a);
        }
        function k(e) {
          (this.tryEntries = [{ tryLoc: "root" }]),
            e.forEach(S, this),
            this.reset(!0);
        }
        function P(e) {
          if (e) {
            var a = e[o];
            if (a) return a.call(e);
            if ("function" === typeof e.next) return e;
            if (!isNaN(e.length)) {
              var r = -1,
                l = function a() {
                  for (; ++r < e.length; )
                    if (n.call(e, r)) return (a.value = e[r]), (a.done = !1), a;
                  return (a.value = t), (a.done = !0), a;
                };
              return (l.next = l);
            }
          }
          return { next: U };
        }
        function U() {
          return { value: t, done: !0 };
        }
      })(
        (function() {
          return this || ("object" === typeof self && self);
        })() || Function("return this")()
      );
    },
    133: function(e, a, t) {},
    134: function(e, a, t) {},
    135: function(e, a, t) {},
    141: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = t(34);
      t(133);
      a.default = function() {
        return n.a.createElement(
          l.a,
          { id: "categorias", title: "Categor\xedas" },
          n.a.createElement(
            "div",
            { className: "row" },
            n.a.createElement(
              "div",
              { className: "col-12" },
              n.a.createElement(
                "h4",
                { className: "categorias-title" },
                "Categor\xedas"
              ),
              n.a.createElement(
                "p",
                { className: "categorias-subtitle text-center" },
                "Los participantes se pueden inscribir en equipos masculinos, femeninos o mixtos. Las categor\xedas disponiles son las siguientes"
              )
            )
          ),
          n.a.createElement(
            "div",
            { className: "row mb-5" },
            n.a.createElement(
              "div",
              { className: "col-12 col-md-4 categoria-type p-5" },
              n.a.createElement(
                "h5",
                null,
                "Categor\xeda",
                n.a.createElement("br", null),
                " \xc9lite"
              )
            ),
            n.a.createElement(
              "div",
              { className: "col-12 col-md-8 categoria-description p-5" },
              n.a.createElement(
                "ul",
                null,
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Elite masculino:"),
                  " Una pareja compuesta por dos hombres que debe tener 19 a\xf1os o m\xe1s el 31 de diciembre del a\xf1o en que se celebra la competici\xf3n. Debe poseer una licencia Elite homologada."
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Elite femenina:"),
                  " una pareja compuesta por dos mujeres que debe tener 19 a\xf1os o m\xe1s el 31 de diciembre del a\xf1o en que se celebra la competici\xf3n. Debe poseer una licencia Elite homologada."
                )
              )
            )
          ),
          n.a.createElement(
            "div",
            { className: "row mb-5" },
            n.a.createElement(
              "div",
              {
                className:
                  "col-12 col-md-8 categoria-description p-5 order-2 order-md-1"
              },
              n.a.createElement(
                "ul",
                null,
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Master 30:"),
                  " Una pareja compuesta por dos hombre que debe tener 30 a\xf1os o m\xe1s el 31 de diciembre del a\xf1o en que se celebra la competici\xf3n. Debe poseer una licencia Master homologada para ciclismo de competici\xf3n. Quienes posean licencia de categor\xeda Elite no podr\xe1n competir en esta categor\xeda."
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Master 40:"),
                  " Una pareja compuesta por dos hombres que debe tener 40 a\xf1os o m\xe1s el 31 de diciembre del a\xf1o en que se celebra la competici\xf3n. Debe poseer una licencia Master homologada para ciclismo de competici\xf3n. Quienes posean licencia de categor\xeda Elite o Master 30 no podr\xe1n competir en esta categor\xeda."
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Master 50:"),
                  " Una pareja compuesta por dos hombres que debe tener 50 a\xf1os o m\xe1s el 31 de diciembre del a\xf1o en que se celebra la competici\xf3n. Debe poseer una licencia Master homologada para ciclismo de competici\xf3n. Quienes posean licencia de categor\xeda Elite, Master 30 o Master 40 no podr\xe1n competir en esta categor\xeda."
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Master femenina:"),
                  " Una pareja compuesta por dos mujeres que debe tener 30 a\xf1os o m\xe1s el 31 de diciembre del a\xf1o en que se celebra la competici\xf3n. Debe poseer una licencia Master homologada para ciclismo de competici\xf3n. Quienes posean licencia de categor\xeda Elite no podr\xe1n competir en esta categor\xeda."
                )
              )
            ),
            n.a.createElement(
              "div",
              {
                className:
                  "col-12 col-md-4 categoria-type p-5 order-1 order-md-2"
              },
              n.a.createElement(
                "h5",
                null,
                "Categor\xeda",
                n.a.createElement("br", null),
                " Master"
              )
            )
          ),
          n.a.createElement(
            "div",
            { className: "row mb-5" },
            n.a.createElement(
              "div",
              { className: "col-12 col-md-4 categoria-type p-5" },
              n.a.createElement(
                "h5",
                null,
                "Categor\xeda",
                n.a.createElement("br", null),
                " Open"
              )
            ),
            n.a.createElement(
              "div",
              { className: "col-12 col-md-8 categoria-description p-5" },
              n.a.createElement(
                "ul",
                null,
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement(
                    "strong",
                    null,
                    "Categor\xeda OPEN PAREJAS MASCULINO:"
                  ),
                  " esta categor\xeda son todos los hombres que deben tener un m\xednimo de 18 a\xf1os . Aqu\xed participar\xe1n aquellas parejas en las que al menos uno de sus miembros tenga Licencia cicloturista, o Licencia de D\xeda o edades diferentes para poder entrar en la categor\xeda master ( ejemplo una persona de 40 a\xf1os y una de 30 a\xf1os)"
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement(
                    "strong",
                    null,
                    "Categor\xeda OPEN PAREJAS FEMENINO:"
                  ),
                  " esta categor\xeda son todas las mujeres que deben tener un m\xednimo de 18 a\xf1os . Aqu\xed participar\xe1n aquellas parejas en las que al menos uno de sus miembros tenga Licencia cicloturista, o Licencia de D\xeda."
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement(
                    "strong",
                    null,
                    "Categor\xeda OPEN INDIVIDUAL MASCULINO:"
                  ),
                  " esta categor\xeda son todos los hombres que deben tener un m\xednimo de 18 a\xf1os y que deseen competir de forma individual. Los participantes que no posean licencia federativa y deseen adquirir una licencia de d\xeda, deber\xe1n participar en esta categor\xeda, asi como los que posean licencia cicloturista."
                ),
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement(
                    "strong",
                    null,
                    "Categor\xeda OPEN INDIVIDUAL FEMENINO:"
                  ),
                  " esta categor\xeda son todas las mujeres que deben tener un m\xednimo de 18 a\xf1os y que deseen competir de forma individual. Los participantes que no posean licencia federativa y deseen adquirir una licencia de d\xeda, deber\xe1n participar en esta categor\xeda. As\xed como las que posean licencia cicloturista."
                )
              )
            )
          ),
          n.a.createElement(
            "div",
            { className: "row mb-5" },
            n.a.createElement(
              "div",
              {
                className:
                  "col-12 col-md-8 categoria-description p-5 order-2 order-md-1"
              },
              n.a.createElement(
                "ul",
                null,
                n.a.createElement(
                  "li",
                  null,
                  n.a.createElement("strong", null, "Mixta:"),
                  " En esta categor\xeda participar\xe1n 1 hombre y una mujer con un m\xednimo de 18 a\xf1os cumplidos a 31 de diciembre del a\xf1o en que se celebra la competici\xf3n."
                )
              )
            ),
            n.a.createElement(
              "div",
              {
                className:
                  "col-12 col-md-4 categoria-type p-5 order-1 order-md-2"
              },
              n.a.createElement(
                "h5",
                null,
                "Categor\xeda",
                n.a.createElement("br", null),
                " Mixta"
              )
            )
          )
        );
      };
    },
    142: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = t(15),
        o = t(34);
      t(134);
      a.default = Object(l.connect)(function(e) {
        return { currentUser: e.auth.currentUser };
      }, null)(function(e) {
        e.currentUser;
        return n.a.createElement(
          o.a,
          { id: "participantes", title: "Participantes", noCrawl: !0 },
          n.a.createElement(
            "div",
            { className: "container" },
            n.a.createElement(
              "div",
              { className: "row" },
              n.a.createElement(
                "div",
                { className: "col-12" },
                n.a.createElement(
                  "section",
                  { className: "participantes" },
                  n.a.createElement(
                    "h4",
                    { className: "participantes-title" },
                    "Participantes"
                  ),
                  n.a.createElement(
                    "div",
                    { id: "accordion" },
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingOne" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseOne",
                              "aria-expanded": "false",
                              "aria-controls": "collapseOne"
                            },
                            "\xbfA que tengo derecho con mi inscripci\xf3n?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseOne",
                          className: "collapse",
                          "aria-labelledby": "headingOne",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Los derechos de inscripci\xf3n incluyen: Derecho de participaci\xf3n en la prueba. Avituallamiento l\xedquido y s\xf3lido Maillot conmemorativo del evento Servicio mec\xe1nico en llegadas etapas. Servicio de masajes (opcional)"
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingTwo" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseTwo",
                              "aria-expanded": "false",
                              "aria-controls": "collapseThree"
                            },
                            "\xbfCu\xe1ntas etapas tiene la prueba?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseTwo",
                          className: "collapse",
                          "aria-labelledby": "headingTwo",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "La prueba consta de 3 etapas. Una crono nocturna de 4 km y dos etapas maraton."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingFour" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseFour",
                              "aria-expanded": "false",
                              "aria-controls": "collapseThree"
                            },
                            "\xbfExisten puntos de corte en el recorrido?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseFour",
                          className: "collapse",
                          "aria-labelledby": "headingFour",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Si. En cada una de las etapas habr\xe1 un tiempo limite para acabarlas y se establecer\xe1n puntos de control de paso. Sereis informados con antelaci\xf3n de estos tiempos de corte."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingFive" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseFive",
                              "aria-expanded": "false",
                              "aria-controls": "collapseFive"
                            },
                            "\xbfEst\xe1 el recorrido flechado?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseFive",
                          className: "collapse",
                          "aria-labelledby": "headingFive",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Para realizar el recorrido es preciso utilizar gps, pues no est\xe1 balizado. Aun asi, la organizaci\xf3n utilizar\xe1 en los cruces importantes y tambi\xe9n en las zonas complicadas, balizamiento para poder facilitar a los participantes el camino a seguir."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingSix" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseSix",
                              "aria-expanded": "false",
                              "aria-controls": "collapseSix"
                            },
                            "\xbfQu\xe9 pasa si no finalizo una etapa?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseSix",
                          className: "collapse",
                          "aria-labelledby": "headingSix",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "En ese caso, tengo que pasar a categor\xeda individual en la siguiente etapa, saldr\xe9 del \xfaltimo caj\xf3n y no tendr\xe9 finisher de la prueba."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingSeven" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseSeven",
                              "aria-expanded": "false",
                              "aria-controls": "collapseSeven"
                            },
                            "\xbfQu\xe9 pasa si abandona mi compa\xf1er@?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseSeven",
                          className: "collapse",
                          "aria-labelledby": "headingSeven",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "En ese caso, tengo que pasar a categor\xeda individual en la siguiente etapa, saldr\xe9 del \xfaltimo caj\xf3n y SI tendr\xe9 finisher de la prueba en caso de finalizar las 3 etapas."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingEight" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseEight",
                              "aria-expanded": "false",
                              "aria-controls": "collapseEight"
                            },
                            "\xbfLa prueba es abierta al tr\xe1fico?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseEight",
                          className: "collapse",
                          "aria-labelledby": "headingEight",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "SI, ASI ES. La prueba est\xe1 abierta al tr\xe1fico por lo que se deber\xe1 respetar sem\xe1foros, cruces, modo de circulaci\xf3n, etc. La organizaci\xf3n no se responsabiliza de los posibles percances acaecidos por no respetar la circulaci\xf3n y sus normas, pudiendo ser expulsados de la prueba en caso de que se vea a alg\xfan participantes cometiendo una infracci\xf3n."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingNine" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseNine",
                              "aria-expanded": "false",
                              "aria-controls": "collapseNine"
                            },
                            "\xbfEs obligatorio llevar m\xf3vil en la prueba?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseNine",
                          className: "collapse",
                          "aria-labelledby": "headingNine",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Recomendamos llevar m\xf3vil en la prueba. para que la organizaci\xf3n pueda ponerse en contacto si surge alg\xfan imprevisto o hay alg\xfan problema. Importante que tenga bater\xeda suficiente."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingTen" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseTen",
                              "aria-expanded": "false",
                              "aria-controls": "collapseTen"
                            },
                            "\xbfQu\xe9 hago si no decido seguir en la prueba?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseTen",
                          className: "collapse",
                          "aria-labelledby": "headingTen",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Fundamental que de aviso a la organizaci\xf3n lo antes posible para poder tener controlados a todos los participantes y velar por su seguridad."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingEleven" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseEleven",
                              "aria-expanded": "false",
                              "aria-controls": "collapseThree"
                            },
                            "\xbfCu\xe1ndo puedo retirar el dorsal?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseEleven",
                          className: "collapse",
                          "aria-labelledby": "headingEleven",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Se establecer\xe1 con tiempo suficiente el lugar y horas de entrega de dorsales."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "headingTwelve" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapseTwelve",
                              "aria-expanded": "false",
                              "aria-controls": "collapseTwelve"
                            },
                            "\xbfPuede retirar el dorsal alg\xfan compa\xf1ero por m\xed?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapseTwelve",
                          className: "collapse",
                          "aria-labelledby": "headingTwelve",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "S\xed, siempre que tenga DNI y Licencia de la federaci\xf3n, y los papeles que se deben tener cubiertos para la prueba."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "heading13" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapse13",
                              "aria-expanded": "false",
                              "aria-controls": "collapse13"
                            },
                            "\xbfTengo que pagar el seguro federativo?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapse13",
                          className: "collapse",
                          "aria-labelledby": "heading13",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "SOLO si no tienes la licencia Federativa en Vigor. La federaci\xf3n tiene la opci\xf3n de LICENCIA DE D\xcdA para el evento, que en el caso de la Costa Atlantica MTB Tour es de 30 euros por las 3 etapas."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "heading14" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapse14",
                              "aria-expanded": "false",
                              "aria-controls": "collapse14"
                            },
                            "\xbfQu\xe9 hay en los avituallamientos?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapse14",
                          className: "collapse",
                          "aria-labelledby": "heading14",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "En los avituallamientos se dispondr\xe1 de: Avituallamiento s\xf3lido/l\xedquido ( para los participantes): fruta variada, frutos secos, agua, bebida isot\xf3nica, coca cola, pastelitos, etc."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "heading15" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapse15",
                              "aria-expanded": "false",
                              "aria-controls": "collapse15"
                            },
                            "\xbfEs obligatorio recoger el trofeo si lo gano?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapse15",
                          className: "collapse",
                          "aria-labelledby": "heading15",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "SI. Es obligatorio. En cada una de las etapas, los l\xedderes de la general y de cada categor\xeda, recibir\xe1n el maillot de l\xedder y SER\xc1 OBLIGATORIO, que corran con dicho maillot en la siguiente etapa. Los ganadores finales tambi\xe9n deben estar en el p\xf3dium para la recogida de premios el \xfaltimo dia de la prueba."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "heading16" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapse16",
                              "aria-expanded": "false",
                              "aria-controls": "collapse16"
                            },
                            "\xbfHace falta dispositivo GPS?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapse16",
                          className: "collapse",
                          "aria-labelledby": "heading16",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "Si, es preciso en la prueba, aunque si se corre en parejas o se decide ir varias personas juntas, con que una de ellas tenga dicho dispositivo, ser\xe1 suficiente. La prueba tiene puntos de marcaje reforzado para facilitar el guiado."
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "card" },
                      n.a.createElement(
                        "div",
                        { className: "card-header", id: "heading17" },
                        n.a.createElement(
                          "h5",
                          { className: "mb-0" },
                          n.a.createElement(
                            "button",
                            {
                              className: "btn btn-link collapsed",
                              "data-toggle": "collapse",
                              "data-target": "#collapse17",
                              "aria-expanded": "false",
                              "aria-controls": "collapse17"
                            },
                            "\xbfSi no voy se devuelve la cuota que he pagado?"
                          )
                        )
                      ),
                      n.a.createElement(
                        "div",
                        {
                          id: "collapse17",
                          className: "collapse",
                          "aria-labelledby": "heading17",
                          "data-parent": "#accordion"
                        },
                        n.a.createElement(
                          "div",
                          { className: "card-body" },
                          "No hay devoluci\xf3n econ\xf3mica salvo en las fechas y porcentajes marcados por la organizaci\xf3n en el reglamento. EN CASO DE QUE EL COVID impida celebrar la prueba, los participantes podr\xe1n decidir participar en la nueva fecha, o solicitar en soporte@emesports.es la devoluci\xf3n de su tarifa ( menos 2 euros de gastos de gesti\xf3n"
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        );
      });
    },
    143: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = t(15),
        o = t(34);
      a.default = Object(l.connect)(function(e) {
        return { currentUser: e.auth.currentUser };
      }, null)(function(e) {
        e.currentUser;
        return n.a.createElement(
          o.a,
          { id: "dashboard", title: "Dashboard", noCrawl: !0 },
          n.a.createElement("h6", { className: "title" }, "Pruebas GlobalDXT")
        );
      });
    },
    144: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = t(15),
        o = t(34);
      a.default = Object(l.connect)(function(e) {
        return { currentUser: e.auth.currentUser };
      }, null)(function(e) {
        e.currentUser;
        return n.a.createElement(
          o.a,
          { id: "dashboard", title: "Dashboard", noCrawl: !0 },
          n.a.createElement("h6", { className: "title" }, "Pruebas Amigas")
        );
      });
    },
    145: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = t(15),
        o = t(34);
      a.default = Object(l.connect)(function(e) {
        return { currentUser: e.auth.currentUser };
      }, null)(function(e) {
        e.currentUser;
        return n.a.createElement(
          o.a,
          { id: "dashboard", title: "Dashboard", noCrawl: !0 },
          n.a.createElement("h6", { className: "title" }, "Ropa evento")
        );
      });
    },
    146: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = t(15),
        o = t(34);
      a.default = Object(l.connect)(function(e) {
        return { currentUser: e.auth.currentUser };
      }, null)(function(e) {
        e.currentUser;
        return n.a.createElement(
          o.a,
          { id: "dashboard", title: "Dashboard", noCrawl: !0 },
          n.a.createElement("h6", { className: "title" }, "Embajadores")
        );
      });
    },
    147: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(9),
        n = t(10),
        l = t(12),
        o = t(11),
        c = t(13),
        i = t(0),
        s = t(14),
        d = t(15),
        u = t(26),
        m = (function(e) {
          function a() {
            return (
              Object(r.a)(this, a),
              Object(l.a)(this, Object(o.a)(a).apply(this, arguments))
            );
          }
          return (
            Object(c.a)(a, e),
            Object(n.a)(a, [
              {
                key: "componentWillMount",
                value: function() {
                  this.props.logoutUser(), this.props.history.push("/");
                }
              },
              {
                key: "render",
                value: function() {
                  return null;
                }
              }
            ]),
            a
          );
        })(i.Component);
      a.default = Object(d.connect)(null, function(e) {
        return Object(s.b)({ logoutUser: u.c }, e);
      })(m);
    },
    148: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(0),
        n = t.n(r),
        l = (t(15), t(34));
      t(135);
      a.default = function() {
        return n.a.createElement(
          l.a,
          { id: "paquetes", title: "Paquetes", noCrawl: !0 },
          n.a.createElement(
            "div",
            { className: "row" },
            n.a.createElement(
              "div",
              { className: "col-12" },
              n.a.createElement(
                "h4",
                { className: "paquetes-title" },
                "Paquetes"
              ),
              n.a.createElement(
                "p",
                { className: "paquetes-subtitle text-center" },
                "Adem\xe1s de la inscripci\xf3n, la organizaci\xf3n ofrece varios paquetes que pueden ayudar a tener una mejor experiencia de la prueba."
              )
            )
          ),
          n.a.createElement(
            "div",
            { className: "row mb-5" },
            n.a.createElement(
              "div",
              {
                className:
                  "col-12 col-md-8 paquete-description p-5 order-2 order-md-1"
              },
              n.a.createElement(
                "p",
                null,
                "Al final de la 2\xaa y 3\xaa etapa y por riguroso orden de llegada al set de masaje, las personas que elijan este servicio recibir\xe1n un masaje de 20 minutos que les permitir\xe1 afrontar la siguiente etapa con \xe9xito."
              )
            ),
            n.a.createElement(
              "div",
              {
                className: "col-12 col-md-4 paquete-type p-5 order-1 order-md-2"
              },
              n.a.createElement("h5", null, "Servicio de masaje"),
              n.a.createElement("h6", null, "29\u20ac")
            )
          )
        );
      };
    },
    150: function(e, a, t) {
      "use strict";
      t.r(a);
      var r = t(9),
        n = t(10),
        l = t(12),
        o = t(11),
        c = t(13),
        i = t(130),
        s = t.n(i);
      function d(e, a, t, r, n, l, o) {
        try {
          var c = e[l](o),
            i = c.value;
        } catch (s) {
          return void t(s);
        }
        c.done ? a(i) : Promise.resolve(i).then(r, n);
      }
      var u = t(0),
        m = t.n(u),
        p = t(14),
        h = t(15),
        b = t(37),
        g = t(34),
        f = t(38),
        E = (function() {
          var e,
            a = ((e = s.a.mark(function e(a) {
              return s.a.wrap(function(e) {
                for (;;)
                  switch ((e.prev = e.next)) {
                    case 0:
                      return (
                        (e.next = 2), a.getCurrentProfile(+a.match.params.id)
                      );
                    case 2:
                      return e.abrupt("return", e.sent);
                    case 3:
                    case "end":
                      return e.stop();
                  }
              }, e);
            })),
            function() {
              var a = this,
                t = arguments;
              return new Promise(function(r, n) {
                var l = e.apply(a, t);
                function o(e) {
                  d(l, r, n, o, c, "next", e);
                }
                function c(e) {
                  d(l, r, n, o, c, "throw", e);
                }
                o(void 0);
              });
            });
          return function(e) {
            return a.apply(this, arguments);
          };
        })(),
        v = (function(e) {
          function a() {
            return (
              Object(r.a)(this, a),
              Object(l.a)(this, Object(o.a)(a).apply(this, arguments))
            );
          }
          return (
            Object(c.a)(a, e),
            Object(n.a)(a, [
              {
                key: "componentWillUnmount",
                value: function() {
                  this.props.removeCurrentProfile();
                }
              },
              {
                key: "shouldComponentUpdate",
                value: function(e) {
                  return (
                    e.match.params.id !== this.props.match.params.id &&
                      this.props.getCurrentProfile(+e.match.params.id),
                    !0
                  );
                }
              },
              {
                key: "render",
                value: function() {
                  var e = this.props.currentProfile,
                    a = e.name,
                    t = e.id,
                    r = e.image;
                  return m.a.createElement(
                    g.a,
                    {
                      id: "profile",
                      title: a,
                      description: "This is user profile number ".concat(t),
                      image: r
                    },
                    m.a.createElement(
                      "p",
                      null,
                      m.a.createElement("b", null, "Name:"),
                      " ",
                      a
                    ),
                    m.a.createElement(
                      "p",
                      null,
                      m.a.createElement("b", null, "ID:"),
                      " ",
                      t
                    ),
                    m.a.createElement("img", {
                      src: r,
                      alt: a,
                      style: { width: "400px" }
                    })
                  );
                }
              }
            ]),
            a
          );
        })(u.Component);
      a.default = Object(h.connect)(
        function(e) {
          return { currentProfile: e.profile.currentProfile };
        },
        function(e) {
          return Object(p.b)(
            { getCurrentProfile: f.b, removeCurrentProfile: f.c },
            e
          );
        }
      )(Object(b.frontloadConnect)(E, { onMount: !0, onUpdate: !1 })(v));
    }
  }
]);
//# sourceMappingURL=profile.c60fe6c8.chunk.js.map
