(window.webpackJsonp = window.webpackJsonp || []).push([
  [2],
  {
    100: function(e, a, t) {},
    101: function(e, a) {},
    109: function(e, a) {},
    116: function(e, a) {},
    117: function(e, a, t) {
      e.exports = t.p + "static/media/rightArrow.b74e2b31.svg";
    },
    118: function(e, a, t) {
      e.exports = t.p + "static/media/hero.434b695b.png";
    },
    119: function(e, a, t) {},
    149: function(e, a, t) {
      "use strict";
      t.r(a);
      var n = t(0),
        r = t.n(n),
        l = t(87),
        s = t(53),
        c = t(34),
        o = t(9),
        i = t(10),
        m = t(12),
        d = t(11),
        u = t(13),
        p = (t(92),
        (function(e) {
          function a(e) {
            var t;
            return (
              Object(o.a)(this, a),
              ((t = Object(m.a)(this, Object(d.a)(a).call(this, e))).state = {
                days: 0,
                hours: 0,
                min: 0,
                sec: 0
              }),
              t
            );
          }
          return (
            Object(u.a)(a, e),
            Object(i.a)(a, [
              {
                key: "componentDidMount",
                value: function() {
                  var e = this;
                  this.interval = setInterval(function() {
                    var a = e.calculateCountdown(e.props.date);
                    a ? e.setState(a) : e.stop();
                  }, 1e3);
                }
              },
              {
                key: "calculateCountdown",
                value: function(e) {
                  var a =
                    (Date.parse(new Date(e)) - Date.parse(new Date())) / 1e3;
                  if (a <= 0) return !1;
                  var t = {
                    years: 0,
                    days: 0,
                    hours: 0,
                    min: 0,
                    sec: 0,
                    millisec: 0
                  };
                  return (
                    a >= 31557600 &&
                      ((t.years = Math.floor(a / 31557600)),
                      (a -= 365.25 * t.years * 86400)),
                    a >= 86400 &&
                      ((t.days = Math.floor(a / 86400)), (a -= 86400 * t.days)),
                    a >= 3600 &&
                      ((t.hours = Math.floor(a / 3600)), (a -= 3600 * t.hours)),
                    a >= 60 &&
                      ((t.min = Math.floor(a / 60)), (a -= 60 * t.min)),
                    (t.sec = a),
                    t
                  );
                }
              },
              {
                key: "stop",
                value: function() {
                  clearInterval(this.interval);
                }
              },
              {
                key: "addLeadingZeros",
                value: function(e) {
                  for (e = String(e); e.length < 2; ) e = "0" + e;
                  return e;
                }
              },
              {
                key: "render",
                value: function() {
                  var e = this.state;
                  return r.a.createElement(
                    "div",
                    { className: "Countdown" },
                    r.a.createElement(
                      "span",
                      { className: "Countdown-col" },
                      r.a.createElement(
                        "span",
                        { className: "Countdown-col-element" },
                        r.a.createElement(
                          "strong",
                          null,
                          this.addLeadingZeros(e.days)
                        ),
                        r.a.createElement(
                          "span",
                          null,
                          1 === e.days ? "D\xeda" : "D\xedas"
                        )
                      )
                    ),
                    r.a.createElement(
                      "span",
                      { className: "Countdown-col" },
                      r.a.createElement(
                        "span",
                        { className: "Countdown-col-element" },
                        r.a.createElement(
                          "strong",
                          null,
                          this.addLeadingZeros(e.hours)
                        ),
                        r.a.createElement("span", null, "Horas")
                      )
                    ),
                    r.a.createElement(
                      "span",
                      { className: "Countdown-col" },
                      r.a.createElement(
                        "span",
                        { className: "Countdown-col-element" },
                        r.a.createElement(
                          "strong",
                          null,
                          this.addLeadingZeros(e.min)
                        ),
                        r.a.createElement("span", null, "Minutos")
                      )
                    ),
                    r.a.createElement(
                      "span",
                      { className: "Countdown-col" },
                      r.a.createElement(
                        "span",
                        { className: "Countdown-col-element" },
                        r.a.createElement(
                          "strong",
                          null,
                          this.addLeadingZeros(e.sec)
                        ),
                        r.a.createElement("span", null, "Segundos")
                      )
                    )
                  );
                }
              }
            ]),
            a
          );
        })(n.Component)),
        g = function(e) {
          var a = e.image,
            t = {
              display: "inline-block",
              height: "550px",
              width: "100%",
              backgroundImage: "url(".concat(a, ")"),
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "50% 60%"
            };
          return r.a.createElement("div", { className: "slide", style: t });
        },
        E = t(93),
        f = t.n(E),
        v = function(e) {
          return r.a.createElement(
            "div",
            { className: "right-arrow", onClick: e.goToNextSlide },
            r.a.createElement("img", { src: f.a, alt: "arrow" })
          );
        },
        N = t(94),
        h = t.n(N),
        b = function(e) {
          return r.a.createElement(
            "div",
            { className: "left-arrow", onClick: e.goToPrevSlide },
            r.a.createElement("img", { src: h.a, alt: "arrow" })
          );
        },
        w = t(95),
        y = t.n(w),
        C = t(96),
        k = t.n(C),
        x = t(97),
        S = t.n(x),
        j = t(98),
        H = t.n(j),
        T = t(99),
        I = t.n(T),
        O = (t(100),
        t(136),
        (function(e) {
          function a(e) {
            var t;
            return (
              Object(o.a)(this, a),
              ((t = Object(m.a)(
                this,
                Object(d.a)(a).call(this, e)
              )).componentDidMount = function() {
                var e = window.setInterval(function() {
                  t.goToNextSlide();
                }, 3e3);
                t.setState({ interval: e });
              }),
              (t.goToPrevSlide = function() {}),
              (t.goToNextSlide = function() {
                if (t.state.currentIndex === t.state.images.length - 1)
                  return t.setState({ currentIndex: 0, translateValue: 0 });
                t.setState(function(e) {
                  return {
                    currentIndex: e.currentIndex + 1,
                    translateValue: e.translateValue + -t.slideWidth()
                  };
                });
              }),
              (t.slideWidth = function() {
                return document.querySelector(".slide").clientWidth;
              }),
              (t.state = {
                images: [y.a, k.a, S.a, H.a],
                currentIndex: 0,
                interval: null
              }),
              t
            );
          }
          return (
            Object(u.a)(a, e),
            Object(i.a)(a, [
              {
                key: "render",
                value: function() {
                  return r.a.createElement(
                    "div",
                    { className: "slider" },
                    r.a.createElement(
                      "div",
                      {
                        className: "slider-wrapper",
                        style: {
                          transform: "translateX(".concat(
                            this.state.translateValue,
                            "px)"
                          )
                        }
                      },
                      this.state.images.map(function(e, a) {
                        return r.a.createElement(g, { key: a, image: e });
                      })
                    ),
                    r.a.createElement("div", { className: "slider-black" }),
                    r.a.createElement("div", {
                      className: "slider-overlay",
                      style: { backgroundImage: "url(".concat(I.a, ")") }
                    }),
                    r.a.createElement(
                      "div",
                      { className: "slider-caption" },
                      r.a.createElement(
                        "h1",
                        null,
                        "Costa Atl\xe1ntica MTB Tour"
                      ),
                      r.a.createElement(
                        "h5",
                        null,
                        "24, 25 y 26 de septiembre"
                      ),
                      r.a.createElement(
                        "p",
                        null,
                        "Inscripciones abiertas a partir del 2 de abril de 2021"
                      ),
                      r.a.createElement(
                        "p",
                        { className: "gps" },
                        "NAVEGACI\xd3N GPS 100 %"
                      )
                    ),
                    r.a.createElement(
                      s.a,
                      { query: { minWidth: 768 } },
                      function(e) {
                        return e
                          ? r.a.createElement(p, {
                              date: "2021-09-25T00:00:00"
                            })
                          : "";
                      }
                    ),
                    r.a.createElement(b, { goToPrevSlide: this.goToPrevSlide }),
                    r.a.createElement(v, { goToNextSlide: this.goToNextSlide })
                  );
                }
              }
            ]),
            a
          );
        })(r.a.Component)),
        L = t(117),
        A = t.n(L);
      t(118),
        t(119),
        (a.default = function() {
          return r.a.createElement(
            c.a,
            { id: "homepage" },
            r.a.createElement(O, null),
            r.a.createElement(
              "section",
              { className: "Home-countdown" },
              r.a.createElement(s.a, { query: { minWidth: 768 } }, function(e) {
                return e
                  ? ""
                  : r.a.createElement(p, { date: "2021-09-25T00:00:00" });
              })
            ),
            r.a.createElement(
              "section",
              { className: "Home-pager" },
              r.a.createElement(
                "div",
                { className: "container-fluid" },
                r.a.createElement(
                  "div",
                  { className: "row" },
                  r.a.createElement(
                    "div",
                    { className: "col-md-12 col-lg-4" },
                    r.a.createElement(
                      l.a,
                      { className: "Home-pager-link", to: "" },
                      r.a.createElement(
                        "h2",
                        { className: "Home-pager-title" },
                        "Prueba en directo"
                      ),
                      r.a.createElement(
                        "h5",
                        { className: "Home-pager-subtitle" },
                        "Sigue al minuto la prueba trav\xe9s de las redes sociales"
                      ),
                      r.a.createElement(
                        "span",
                        { className: "Home-pager-arrow-wrapper" },
                        r.a.createElement("img", {
                          className: "Home-pager-arrow",
                          src: A.a,
                          alt: "arrow"
                        })
                      )
                    )
                  ),
                  r.a.createElement(
                    "div",
                    { className: "col-md-12 col-lg-4" },
                    r.a.createElement(
                      "a",
                      {
                        className: "Home-pager-link",
                        href:
                          "https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/participantes/",
                        target: "_blank",
                        rel: "noopener noreferrer"
                      },
                      r.a.createElement(
                        "h2",
                        { className: "Home-pager-title" },
                        "Inscritos"
                      ),
                      r.a.createElement(
                        "h5",
                        { className: "Home-pager-subtitle" },
                        "Consulta la lista de inscritos"
                      ),
                      r.a.createElement(
                        "span",
                        { className: "Home-pager-arrow-wrapper" },
                        r.a.createElement("img", {
                          className: "Home-pager-arrow",
                          src: A.a,
                          alt: "arrow"
                        })
                      )
                    )
                  ),
                  r.a.createElement(
                    "div",
                    { className: "col-md-12 col-lg-4" },
                    r.a.createElement(
                      l.a,
                      { className: "Home-pager-link", to: "" },
                      r.a.createElement(
                        "h2",
                        { className: "Home-pager-title" },
                        "Resultados de la prueba"
                      ),
                      r.a.createElement(
                        "h5",
                        { className: "Home-pager-subtitle" },
                        "Comprueba tu posici\xf3n de esta edici\xf3n"
                      ),
                      r.a.createElement(
                        "span",
                        { className: "Home-pager-arrow-wrapper" },
                        r.a.createElement("img", {
                          className: "Home-pager-arrow",
                          src: A.a,
                          alt: "arrow"
                        })
                      )
                    )
                  )
                )
              )
            ),
            r.a.createElement(
              "div",
              { className: "container" },
              r.a.createElement(
                "div",
                { className: "row" },
                r.a.createElement(
                  "div",
                  { className: "col-12" },
                  r.a.createElement(
                    "section",
                    { className: "description" },
                    r.a.createElement(
                      "h4",
                      { className: "description-title" },
                      "Costa Atl\xe1ntica MTB Tour"
                    ),
                    r.a.createElement(
                      "h6",
                      { className: "description-text" },
                      "La ",
                      r.a.createElement(
                        "strong",
                        null,
                        "Costa Atl\xe1ntica MTB Tour"
                      ),
                      ", es una competici\xf3n por etapas de bicicleta de monta\xf1a. los participantes disfrutar\xe1n de 3 etapas: una crono nocturna y dos etapas marat\xf3n. La prueba tiene un ",
                      r.a.createElement("strong", null, " formato CHALLENGE"),
                      ", lo que quiere decir que cada etapa se disputa de forma independiente, por lo que aunque un participante/pareja no finalice alguna de las etapas, podr\xe1n disputar el resto. Si un participante/pareja, no finaliza una etapa,",
                      " ",
                      r.a.createElement(
                        "strong",
                        null,
                        r.a.createElement(
                          "u",
                          null,
                          "en la general se computar\xe1 en dicha etapa, el tiempo del \xfaltimo participante/pareja."
                        )
                      ),
                      r.a.createElement("br", null),
                      r.a.createElement("br", null),
                      "Los participantes se enfrentar\xe1n tanto de forma individual como por parejas en cualquiera de las dos modalidades. La prueba comienza el viernes 24 de septiembre de 2020 a las 20:00 con una CRONO NOCTURNA en Pontevedra (en esta prueba, se decidir\xe1n los cajones del d\xeda siguiente,), primera etapa se desarrolla el s\xe1bado 25, con salida a las 9:30 de la ma\xf1ana desde la localidad de Pontevedra, la segunda etapa tiene salida el domingo 26 en la localidad a determinar a las 9:30 horas, dando por finalizada la prueba.",
                      r.a.createElement("br", null),
                      r.a.createElement("br", null),
                      "La competici\xf3n se disputa seg\xfan el reglamento t\xe9cnico y deportivo de la Real Federaci\xf3n Espa\xf1ola de Ciclismo (RFEC)."
                    )
                  )
                )
              )
            ),
            r.a.createElement(
              "section",
              { className: "video" },
              r.a.createElement(
                "div",
                { className: "container" },
                r.a.createElement(
                  "div",
                  { className: "row" },
                  r.a.createElement(
                    "div",
                    { className: "col-12 col-md-8 offset-md-2" },
                    r.a.createElement(
                      "div",
                      { class: "embed-responsive embed-responsive-16by9" },
                      r.a.createElement("iframe", {
                        width: "560",
                        height: "315",
                        src: "https://www.youtube.com/embed/px4Nk9d1_dA",
                        frameborder: "0",
                        allow:
                          "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture",
                        allowfullscreen: !0
                      })
                    )
                  )
                )
              )
            )
          );
        });
    },
    92: function(e, a, t) {},
    93: function(e, a, t) {
      e.exports = t.p + "static/media/rightArrow.9c59704b.svg";
    },
    94: function(e, a, t) {
      e.exports = t.p + "static/media/leftArrow.5727f869.svg";
    },
    95: function(e, a, t) {
      e.exports = t.p + "static/media/slide1.85dff35e.jpg";
    },
    96: function(e, a, t) {
      e.exports = t.p + "static/media/slide2.33085c90.jpg";
    },
    97: function(e, a, t) {
      e.exports = t.p + "static/media/slide3.22d96a5b.jpg";
    },
    98: function(e, a, t) {
      e.exports = t.p + "static/media/slide4.dcdc8bca.jpg";
    },
    99: function(e, a, t) {
      e.exports = t.p + "static/media/diagonalBlack.2b4c41fd.svg";
    }
  }
]);
//# sourceMappingURL=homepage.5f8f83b4.chunk.js.map
