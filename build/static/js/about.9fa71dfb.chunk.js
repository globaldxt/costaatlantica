(window.webpackJsonp = window.webpackJsonp || []).push([
  [0],
  {
    120: function(e, a, t) {
      e.exports = t.p + "static/media/etapa1.660645f7.jpg";
    },
    121: function(e, a, t) {
      e.exports = t.p + "static/media/etapa2.15a4d1b5.jpg";
    },
    122: function(e, a, t) {
      e.exports = t.p + "static/media/etapa3.70d7f9e7.jpg";
    },
    123: function(e, a, t) {},
    137: function(e, a, t) {
      "use strict";
      t.r(a);
      var s = t(0),
        n = t.n(s),
        r = t(34),
        l = t(120),
        c = t.n(l),
        i = t(121),
        o = t.n(i),
        d = t(122),
        m = t.n(d);
      t(123);
      a.default = function() {
        return n.a.createElement(
          r.a,
          { id: "etapas", title: "Etapas" },
          n.a.createElement(
            "div",
            { className: "container-fluid" },
            n.a.createElement(
              "div",
              { className: "row" },
              n.a.createElement(
                "div",
                { className: "col-12" },
                n.a.createElement(
                  "section",
                  { className: "etapas" },
                  n.a.createElement(
                    "h4",
                    { className: "etapas-title" },
                    "Etapas"
                  ),
                  n.a.createElement(
                    "div",
                    { className: "row no-gutters" },
                    n.a.createElement(
                      "div",
                      { className: "col-12 col-sm-7" },
                      n.a.createElement("img", {
                        className: "img-fluid img-etapa1",
                        src: c.a,
                        alt: "image"
                      })
                    ),
                    n.a.createElement(
                      "div",
                      { className: "col-12 col-sm-5" },
                      n.a.createElement(
                        "div",
                        { className: "etapa-info" },
                        n.a.createElement(
                          "h5",
                          null,
                          "Etapa 1: Crono nocturna"
                        ),
                        n.a.createElement(
                          "div",
                          { className: "etapa-datos" },
                          n.a.createElement("p", null, "4km aprox.")
                        ),
                        n.a.createElement(
                          "p",
                          { className: "etapa-info-text" },
                          "Se disputar\xe1 en Pontevedra el viernes 24 de septiembre a las 20:00 en un recorrido urbano por su zona vieja, con una distancia aproximada de 4km. Ser\xe1 un recorrido Trepidante, divertido y que pondr\xe1 el coraz\xf3n a las m\xe1ximas pulsaciones.",
                          " ",
                          n.a.createElement("br", null),
                          "Los participantes saldr\xe1n de 4 en 4 con una distancia de 1 minuto entre cada salida. El circuito se abrir\xe1 a las 19: 00 horas para que los participantes puedan reconocerlo. ",
                          n.a.createElement("br", null),
                          "En esta crono, los participantes se disputar\xe1n el caj\xf3n de la primera etapa."
                        )
                      )
                    )
                  ),
                  n.a.createElement(
                    "div",
                    { className: "row no-gutters" },
                    n.a.createElement(
                      "div",
                      { className: "col-12 col-sm-5 order-2 order-md-1" },
                      n.a.createElement(
                        "div",
                        { className: "etapa-info" },
                        n.a.createElement(
                          "h5",
                          null,
                          "Etapa 2: Comienza la aventura"
                        ),
                        n.a.createElement(
                          "div",
                          { className: "etapa-datos" },
                          n.a.createElement("p", null, "80/90km")
                        ),
                        n.a.createElement(
                          "p",
                          { className: "etapa-info-text" },
                          "Ser\xe1 una etapa de 80/90 km de distancia que saldr\xe1 y llegar\xe1 al coraz\xf3n de Pontevedra. Una etapa en la que los bikers podr\xe1n disfrutar de todo tipo de terreno, subidas de infarto, senderos infinitos pegados a la costa, bajadas r\xe1pidas que se entrelazan con otras ratoneras, y una bajada final que dejar\xe1 a todo el mundo con una sonrisa. ",
                          n.a.createElement("br", null),
                          "Esta etapa es la se\xf1a de identidad de la prueba y en sus cerca de 90 km permitir\xe1 a todos los corredores saborear la costa muy de cerca, casi tanto que el agua puede ba\xf1ar sus ruedas. ",
                          n.a.createElement("br", null)
                        )
                      )
                    ),
                    n.a.createElement(
                      "div",
                      { className: "col-12 col-sm-7 order-1 order-md-2" },
                      n.a.createElement("img", {
                        className: "img-fluid img-etapa2",
                        src: o.a,
                        alt: "image"
                      })
                    )
                  ),
                  n.a.createElement(
                    "div",
                    { className: "row no-gutters" },
                    n.a.createElement(
                      "div",
                      { className: "col-12 col-sm-7" },
                      n.a.createElement("img", {
                        className: "img-fluid img-etapa3",
                        src: m.a,
                        alt: "image"
                      })
                    ),
                    n.a.createElement(
                      "div",
                      { className: "col-12 col-sm-5" },
                      n.a.createElement(
                        "div",
                        { className: "etapa-info" },
                        n.a.createElement(
                          "h5",
                          null,
                          "Etapa 3: El fin de la \xe9pica"
                        ),
                        n.a.createElement(
                          "div",
                          { className: "etapa-datos" },
                          n.a.createElement("p", null, "70km")
                        ),
                        n.a.createElement(
                          "p",
                          { className: "etapa-info-text" },
                          "Ser\xe1 una etapa de 70 km de distancia en la que el lugar de salida est\xe1 aun por definir. Las panor\xe1micas espectaculares de toda la costa ser\xe1 una de las se\xf1as de identidad de esta etapa, en la que seguiremos disfrutando de un terreno variado y divertido."
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        );
      };
    }
  }
]);
//# sourceMappingURL=about.9fa71dfb.chunk.js.map
