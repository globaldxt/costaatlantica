(window.webpackJsonp = window.webpackJsonp || []).push([
  [4],
  {
    128: function(e, a, t) {},
    129: function(e, a, t) {
      e.exports = t.p + "static/media/reglamento.e60fd7b0.pdf";
    },
    140: function(e, a, t) {
      "use strict";
      t.r(a);
      var n = t(0),
        r = t.n(n),
        l = t(15),
        c = t(34),
        o = (t(128), t(129)),
        s = t.n(o);
      a.default = Object(l.connect)(function(e) {
        return { currentUser: e.auth.currentUser };
      }, null)(function(e) {
        e.currentUser;
        return r.a.createElement(
          c.a,
          { id: "reglamento", title: "Reglamento", noCrawl: !0 },
          r.a.createElement(
            "div",
            { className: "container" },
            r.a.createElement(
              "div",
              { className: "row" },
              r.a.createElement(
                "div",
                { className: "col-12" },
                r.a.createElement(
                  "section",
                  { className: "reglamento text-center" },
                  r.a.createElement(
                    "h4",
                    { className: "reglamento-title" },
                    "Reglamento"
                  ),
                  r.a.createElement(
                    "p",
                    null,
                    "La COSTA ATL\xc1NTICA MTB TOUR, es una competici\xf3n por etapas de bicicleta de monta\xf1a, dividida en tres etapas marat\xf3n en la que los participantes se enfrentar\xe1n tanto de forma individual como por parejas."
                  ),
                  r.a.createElement(
                    "p",
                    null,
                    "La competici\xf3n se disputa seg\xfan el reglamento t\xe9cnico y deportivo de la Real Federaci\xf3n Espa\xf1ola de Ciclismo (RFEC)."
                  ),
                  r.a.createElement(
                    "p",
                    null,
                    "Puedes descargar el reglamento completo desde este enlace"
                  ),
                  r.a.createElement(
                    "a",
                    {
                      className: "btn btn-primary",
                      href: s.a,
                      target: "_blank",
                      rel: "noopener noreferrer"
                    },
                    "Reglamento"
                  )
                )
              )
            )
          )
        );
      });
    }
  }
]);
//# sourceMappingURL=logout.9411ce4a.chunk.js.map
