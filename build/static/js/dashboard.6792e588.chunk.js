(window.webpackJsonp = window.webpackJsonp || []).push([
  [1],
  {
    124: function(e, a, l) {
      e.exports = l.p + "static/media/tarifas.af0e97a3.png";
    },
    125: function(e, a, l) {},
    138: function(e, a, l) {
      "use strict";
      l.r(a);
      var n = l(0),
        t = l.n(n),
        i = l(34),
        r = l(87),
        s = l(124),
        c = l.n(s);
      l(125);
      a.default = function() {
        return t.a.createElement(
          i.a,
          { id: "tarifas", title: "Tarifas" },
          t.a.createElement(
            "div",
            { className: "row no-gutters" },
            t.a.createElement(
              "div",
              { className: "col-12" },
              t.a.createElement(
                "section",
                { className: "tarifas" },
                t.a.createElement(
                  "div",
                  { className: "row no-gutters" },
                  t.a.createElement(
                    "div",
                    { className: "col-12 col-sm-6" },
                    t.a.createElement(
                      "div",
                      { className: "lista-tarifas" },
                      t.a.createElement(
                        "h4",
                        { className: "tarifas-title" },
                        "Tarifas"
                      ),
                      t.a.createElement(
                        "h6",
                        null,
                        "El plazo de inscripci\xf3n concluye el d\xeda",
                        " ",
                        t.a.createElement("strong", null, "9 de septiembre"),
                        " o hasta agotar las 600 plazas disponibles. ",
                        t.a.createElement("br", null),
                        t.a.createElement("br", null),
                        "El precio de inscripci\xf3n va variando en funci\xf3n del n\xba de inscritos seg\xfan se especifica:",
                        " "
                      ),
                      t.a.createElement(
                        "ul",
                        { className: "mb-5" },
                        t.a.createElement(
                          "li",
                          null,
                          "Del ",
                          t.a.createElement("span", null, "1 al 300."),
                          " Estas plazas ser\xe1n ofertadas SOLO a los participantes de la primera edici\xf3n durante los d\xedas 31 de marzo y 1 de abril. En caso de no cubrirse todas las plazas, se dispondr\xe1 de ellas en el \xfaltimo tramo.",
                          t.a.createElement("strong", null, " 99 \u20ac"),
                          " IVA incluido"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Del ",
                          t.a.createElement("span", null, "301 al 400"),
                          " deber\xe1n abonar por participante",
                          " ",
                          t.a.createElement("strong", null, "110 \u20ac"),
                          " IVA incluido"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Del ",
                          t.a.createElement("span", null, "401 al 500"),
                          " deber\xe1n abonar",
                          " ",
                          t.a.createElement("strong", null, "125 \u20ac"),
                          " por participante IVA incluido"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Del ",
                          t.a.createElement("span", null, "501 al 600"),
                          " deber\xe1n abonar",
                          " ",
                          t.a.createElement("strong", null, "140 \u20ac"),
                          " por participante IVA incluido. Aquellas plazas sobrantes del primer tramo, se a\xf1adir\xe1n en este \xfaltimo tramo."
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "*En caso de no cubrirse la plazas de los participantes de la pasada edici\xf3n, se ofertar\xe1n al resto de los participantes, en la \xfaltima tarifa."
                        )
                      ),
                      t.a.createElement("br", null),
                      t.a.createElement("br", null),
                      t.a.createElement(
                        "h6",
                        null,
                        "LA ",
                        t.a.createElement(
                          "strong",
                          null,
                          "POLITICA DE CANCELACI\xd3N"
                        ),
                        " se realizar\xe1 por devoluciones de la cantidad abonada en funci\xf3n de los siguientes plazos"
                      ),
                      t.a.createElement(
                        "ul",
                        null,
                        t.a.createElement(
                          "li",
                          null,
                          "100% Hasta el 1 de julio de 2021 (menos 2 euros de gastos de gesti\xf3n)"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "50% desde el 1 de julio al 31 de julio de 2021"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "25% desde el 1 de agosto al 25 de agosto de 2021"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "0% a partir del 26 de agosto de 2021"
                        )
                      ),
                      t.a.createElement("br", null),
                      t.a.createElement("br", null),
                      t.a.createElement(
                        "h6",
                        null,
                        "En caso de que",
                        " ",
                        t.a.createElement(
                          "strong",
                          null,
                          "LAS CONDICIONES SANITARIAS ORIGINADAS POR LA COVID 19"
                        ),
                        ", impidan realizar la prueba en la fecha indicada, se anunciar\xe1 una nueva fecha. Aquellos participantes que no puedan asistir a esa fecha podr\xe1n:"
                      ),
                      t.a.createElement(
                        "ul",
                        null,
                        t.a.createElement(
                          "li",
                          null,
                          "Guardar su inscripci\xf3n para 2022"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Solicitar devoluci\xf3n del importe de la inscripci\xf3n ( menos dos euros de gastos de gesti\xf3n) enviando un mail a soporte@emesports.es. El plazo m\xe1ximo para solicitar la devoluci\xf3n es de 1 mes despu\xe9s del anuncio del aplazamiento de prueba."
                        )
                      ),
                      t.a.createElement("br", null),
                      t.a.createElement("br", null),
                      t.a.createElement(
                        "h6",
                        null,
                        t.a.createElement(
                          "strong",
                          null,
                          "En caso de cualquier tipo de descalificaci\xf3n o abandono de la competici\xf3n, la cuota de inscripci\xf3n no ser\xe1 reembolsada ni total ni parcialmente"
                        )
                      )
                    )
                  ),
                  t.a.createElement(
                    "div",
                    { className: "col-12 col-sm-6" },
                    t.a.createElement(
                      "div",
                      { className: "que-incluye" },
                      t.a.createElement(
                        "h6",
                        null,
                        "La inscripci\xf3n incluye"
                      ),
                      t.a.createElement(
                        "ul",
                        null,
                        t.a.createElement(
                          "li",
                          null,
                          "Maillot personalizado del evento"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Atenci\xf3n al participante"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Derecho de participaci\xf3n en la competici\xf3n."
                        ),
                        t.a.createElement("li", null, "Placa numerada."),
                        t.a.createElement(
                          "li",
                          null,
                          "Adhesivos con los perfiles de etapa."
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Obsequios de inscripci\xf3n"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Regalo Finisher (si se logra)"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Dispositivo electr\xf3nico para el control de tiempos"
                        ),
                        t.a.createElement("li", null, "Cronometraje"),
                        t.a.createElement(
                          "li",
                          null,
                          "Marcaje de recorrido y personal"
                        ),
                        t.a.createElement("li", null, "Veh\xedculos escoba"),
                        t.a.createElement(
                          "li",
                          null,
                          "Avituallamientos l\xedquidos y s\xf3lidos"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Pasta party en la 1\xba, 2\xba etapa."
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Asistencia m\xe9dica dentro y fuera del recorrido."
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Asistencia mec\xe1nica en aviutallamiento y final de prueba"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "\xc1rea de lavado de bicicletas."
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Aparcamiento de bicicletas."
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Vestuarios, aseos y duchas( en caso de que los protocolos Covid as\xed lo permitan)"
                        ),
                        t.a.createElement(
                          "li",
                          null,
                          "Otros servicios gratuitos en el Paddock. "
                        )
                      )
                    )
                  )
                )
              ),
              t.a.createElement(
                "section",
                { className: "paquetes-banner" },
                t.a.createElement("img", {
                  className: "img-fluid",
                  src: c.a,
                  alt: "imagen"
                }),
                t.a.createElement("div", { className: "overlay" }),
                t.a.createElement(
                  "div",
                  { className: "paquetes-banner-info" },
                  t.a.createElement("h5", null, "Paquetes de servicios"),
                  t.a.createElement(
                    "p",
                    null,
                    "Conoce los paquetes de servicios ofrecidos junto con la inscripci\xf3n."
                  ),
                  t.a.createElement(
                    r.a,
                    { className: "btn btn-primary", to: "/paquetes" },
                    "Paquetes"
                  )
                )
              )
            )
          )
        );
      };
    }
  }
]);
//# sourceMappingURL=dashboard.6792e588.chunk.js.map
