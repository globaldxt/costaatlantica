import React from "react";
import Page from "../../components/page";
import { Link } from "react-router-dom";

import tarifas from "../../assets/tarifas.png";
import "./tarifas.css";

export default () => (
  <Page id="tarifas" title="Tarifas">
    <div className="row no-gutters">
      <div className="col-12">
        <section className="tarifas">
          <div className="row no-gutters">
            <div className="col-12 col-sm-6">
              <div className="lista-tarifas">
                <h4 className="tarifas-title">Tarifas</h4>
                <h6>
                  El plazo de inscripción concluye el día{" "}
                  <strong>9 de septiembre</strong> o hasta agotar las 600 plazas
                  disponibles. <br />
                  <br />
                  El precio de inscripción va variando en función del nº de
                  inscritos según se especifica:{" "}
                </h6>
                <ul className="mb-5">
                  <li>
                    Del <span>1 al 300.</span> Estas plazas serán ofertadas SOLO
                    a los participantes de la primera edición durante los días
                    31 de marzo y 1 de abril. En caso de no cubrirse todas las
                    plazas, se dispondrá de ellas en el último tramo.
                    <strong> 99 €</strong> IVA incluido
                  </li>
                  <li>
                    Del <span>301 al 400</span> deberán abonar por participante{" "}
                    <strong>110 €</strong> IVA incluido
                  </li>
                  <li>
                    Del <span>401 al 500</span> deberán abonar{" "}
                    <strong>125 €</strong> por participante IVA incluido
                  </li>
                  <li>
                    Del <span>501 al 600</span> deberán abonar{" "}
                    <strong>140 €</strong> por participante IVA incluido.
                    Aquellas plazas sobrantes del primer tramo, se añadirán en
                    este último tramo.
                  </li>
                  <li>
                    *En caso de no cubrirse la plazas de los participantes de la
                    pasada edición, se ofertarán al resto de los participantes,
                    en la última tarifa.
                  </li>
                </ul>
                <br />
                <br />
                <h6>
                  LA <strong>POLITICA DE CANCELACIÓN</strong> se realizará por
                  devoluciones de la cantidad abonada en función de los
                  siguientes plazos
                </h6>
                <ul>
                  <li>
                    100% Hasta el 1 de julio de 2021 (menos 2 euros de gastos de
                    gestión)
                  </li>
                  <li>50% desde el 1 de julio al 31 de julio de 2021</li>
                  <li>25% desde el 1 de agosto al 25 de agosto de 2021</li>
                  <li>0% a partir del 26 de agosto de 2021</li>
                </ul>
                <br />
                <br />
                <h6>
                  En caso de que{" "}
                  <strong>
                    LAS CONDICIONES SANITARIAS ORIGINADAS POR LA COVID 19
                  </strong>
                  , impidan realizar la prueba en la fecha indicada, se
                  anunciará una nueva fecha. Aquellos participantes que no
                  puedan asistir a esa fecha podrán:
                </h6>
                <ul>
                  <li>Guardar su inscripción para 2022</li>
                  <li>
                    Solicitar devolución del importe de la inscripción ( menos
                    dos euros de gastos de gestión) enviando un mail a
                    soporte@emesports.es. El plazo máximo para solicitar la
                    devolución es de 1 mes después del anuncio del aplazamiento
                    de prueba.
                  </li>
                </ul>
                <br />
                <br />
                <h6>
                  <strong>
                    En caso de cualquier tipo de descalificación o abandono de
                    la competición, la cuota de inscripción no será reembolsada
                    ni total ni parcialmente
                  </strong>
                </h6>
              </div>
            </div>
            <div className="col-12 col-sm-6">
              <div className="que-incluye">
                <h6>La inscripción incluye</h6>
                <ul>
                  <li>Maillot personalizado del evento</li>
                  <li>Atención al participante</li>
                  <li>Derecho de participación en la competición.</li>
                  <li>Placa numerada.</li>
                  <li>Adhesivos con los perfiles de etapa.</li>
                  <li>Obsequios de inscripción</li>
                  <li>Regalo Finisher (si se logra)</li>
                  <li>Dispositivo electrónico para el control de tiempos</li>
                  <li>Cronometraje</li>
                  <li>Marcaje de recorrido y personal</li>
                  <li>Vehículos escoba</li>
                  <li>Avituallamientos líquidos y sólidos</li>
                  <li>Pasta party en la 1º, 2º etapa.</li>
                  <li>Asistencia médica dentro y fuera del recorrido.</li>
                  <li>
                    Asistencia mecánica en aviutallamiento y final de prueba
                  </li>
                  <li>Área de lavado de bicicletas.</li>
                  <li>Aparcamiento de bicicletas.</li>
                  <li>
                    Vestuarios, aseos y duchas( en caso de que los protocolos
                    Covid así lo permitan)
                  </li>
                  <li>Otros servicios gratuitos en el Paddock. </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section className="paquetes-banner">
          <img className="img-fluid" src={tarifas} alt="imagen" />
          <div className="overlay" />
          <div className="paquetes-banner-info">
            <h5>Paquetes de servicios</h5>
            <p>
              Conoce los paquetes de servicios ofrecidos junto con la
              inscripción.
            </p>
            <Link className="btn btn-primary" to="/paquetes">
              Paquetes
            </Link>
          </div>
        </section>
      </div>
    </div>
  </Page>
);
