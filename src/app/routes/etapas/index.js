import React from "react";
import Page from "../../components/page";

import etapa1 from "../../assets/etapa1.jpg";
import etapa2 from "../../assets/etapa2.jpg";
import etapa3 from "../../assets/etapa3.jpg";
import "./etapas.css";

export default () => (
  <Page id="etapas" title="Etapas">
    <div className="container-fluid">
      <div className="row">
        <div className="col-12">
          <section className="etapas">
            <h4 className="etapas-title">Etapas</h4>
            <div className="row no-gutters">
              <div className="col-12 col-sm-7">
                <img
                  className="img-fluid img-etapa1"
                  src={etapa1}
                  alt="image"
                />
              </div>
              <div className="col-12 col-sm-5">
                <div className="etapa-info">
                  <h5>Etapa 1: Crono nocturna</h5>
                  <div className="etapa-datos">
                    <p>4km aprox.</p>
                    {/* <p>1300m acumulado</p> */}
                  </div>
                  <p className="etapa-info-text">
                    Se disputará en Pontevedra el viernes 24 de septiembre a las
                    20:00 en un recorrido urbano por su zona vieja, con una
                    distancia aproximada de 4km. Será un recorrido Trepidante,
                    divertido y que pondrá el corazón a las máximas pulsaciones.{" "}
                    <br />
                    Los participantes saldrán de 4 en 4 con una distancia de 1
                    minuto entre cada salida. El circuito se abrirá a las 19: 00
                    horas para que los participantes puedan reconocerlo. <br />
                    En esta crono, los participantes se disputarán el cajón de
                    la primera etapa.
                  </p>
                </div>
              </div>
            </div>
            <div className="row no-gutters">
              <div className="col-12 col-sm-5 order-2 order-md-1">
                <div className="etapa-info">
                  <h5>Etapa 2: Comienza la aventura</h5>
                  <div className="etapa-datos">
                    <p>80/90km</p>
                    {/* <p>1700m acumulado</p> */}
                  </div>
                  <p className="etapa-info-text">
                    Será una etapa de 80/90 km de distancia que saldrá y llegará
                    al corazón de Pontevedra. Una etapa en la que los bikers
                    podrán disfrutar de todo tipo de terreno, subidas de
                    infarto, senderos infinitos pegados a la costa, bajadas
                    rápidas que se entrelazan con otras ratoneras, y una bajada
                    final que dejará a todo el mundo con una sonrisa. <br />
                    Esta etapa es la seña de identidad de la prueba y en sus
                    cerca de 90 km permitirá a todos los corredores saborear la
                    costa muy de cerca, casi tanto que el agua puede bañar sus
                    ruedas. <br />
                  </p>
                </div>
              </div>
              <div className="col-12 col-sm-7 order-1 order-md-2">
                <img
                  className="img-fluid img-etapa2"
                  src={etapa2}
                  alt="image"
                />
              </div>
            </div>
            <div className="row no-gutters">
              <div className="col-12 col-sm-7">
                <img
                  className="img-fluid img-etapa3"
                  src={etapa3}
                  alt="image"
                />
              </div>
              <div className="col-12 col-sm-5">
                <div className="etapa-info">
                  <h5>Etapa 3: El fin de la épica</h5>
                  <div className="etapa-datos">
                    <p>70km</p>
                  </div>
                  <p className="etapa-info-text">
                    Será una etapa de 70 km de distancia en la que el lugar de
                    salida está aun por definir. Las panorámicas espectaculares
                    de toda la costa será una de las señas de identidad de esta
                    etapa, en la que seguiremos disfrutando de un terreno
                    variado y divertido.
                  </p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </Page>
);
