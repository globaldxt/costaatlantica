import React from "react";
import { connect } from "react-redux";
import Page from "../../components/page";

import "./participantes.css";

const Rates = ({ currentUser }) => (
  <Page id="participantes" title="Participantes" noCrawl>
    <div className="container">
      <div className="row">
        <div className="col-12">
          <section className="participantes">
            <h4 className="participantes-title">Participantes</h4>
            <div id="accordion">
              <div className="card">
                <div className="card-header" id="headingOne">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseOne"
                      aria-expanded="false"
                      aria-controls="collapseOne"
                    >
                      ¿A que tengo derecho con mi inscripción?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseOne"
                  className="collapse"
                  aria-labelledby="headingOne"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Los derechos de inscripción incluyen: Derecho de
                    participación en la prueba. Avituallamiento líquido y sólido
                    Maillot conmemorativo del evento Servicio mecánico en
                    llegadas etapas. Servicio de masajes (opcional)
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingTwo">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      ¿Cuántas etapas tiene la prueba?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseTwo"
                  className="collapse"
                  aria-labelledby="headingTwo"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    La prueba consta de 3 etapas. Una crono nocturna de 4 km y
                    dos etapas maraton.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingFour">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseFour"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      ¿Existen puntos de corte en el recorrido?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseFour"
                  className="collapse"
                  aria-labelledby="headingFour"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Si. En cada una de las etapas habrá un tiempo limite para
                    acabarlas y se establecerán puntos de control de paso.
                    Sereis informados con antelación de estos tiempos de corte.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingFive">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseFive"
                      aria-expanded="false"
                      aria-controls="collapseFive"
                    >
                      ¿Está el recorrido flechado?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseFive"
                  className="collapse"
                  aria-labelledby="headingFive"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Para realizar el recorrido es preciso utilizar gps, pues no
                    está balizado. Aun asi, la organización utilizará en los
                    cruces importantes y también en las zonas complicadas,
                    balizamiento para poder facilitar a los participantes el
                    camino a seguir.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingSix">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseSix"
                      aria-expanded="false"
                      aria-controls="collapseSix"
                    >
                      ¿Qué pasa si no finalizo una etapa?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseSix"
                  className="collapse"
                  aria-labelledby="headingSix"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    En ese caso, tengo que pasar a categoría individual en la
                    siguiente etapa, saldré del último cajón y no tendré
                    finisher de la prueba.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingSeven">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseSeven"
                      aria-expanded="false"
                      aria-controls="collapseSeven"
                    >
                      ¿Qué pasa si abandona mi compañer@?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseSeven"
                  className="collapse"
                  aria-labelledby="headingSeven"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    En ese caso, tengo que pasar a categoría individual en la
                    siguiente etapa, saldré del último cajón y SI tendré
                    finisher de la prueba en caso de finalizar las 3 etapas.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingEight">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseEight"
                      aria-expanded="false"
                      aria-controls="collapseEight"
                    >
                      ¿La prueba es abierta al tráfico?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseEight"
                  className="collapse"
                  aria-labelledby="headingEight"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    SI, ASI ES. La prueba está abierta al tráfico por lo que se
                    deberá respetar semáforos, cruces, modo de circulación, etc.
                    La organización no se responsabiliza de los posibles
                    percances acaecidos por no respetar la circulación y sus
                    normas, pudiendo ser expulsados de la prueba en caso de que
                    se vea a algún participantes cometiendo una infracción.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingNine">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseNine"
                      aria-expanded="false"
                      aria-controls="collapseNine"
                    >
                      ¿Es obligatorio llevar móvil en la prueba?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseNine"
                  className="collapse"
                  aria-labelledby="headingNine"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Recomendamos llevar móvil en la prueba. para que la
                    organización pueda ponerse en contacto si surge algún
                    imprevisto o hay algún problema. Importante que tenga
                    batería suficiente.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingTen">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseTen"
                      aria-expanded="false"
                      aria-controls="collapseTen"
                    >
                      ¿Qué hago si no decido seguir en la prueba?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseTen"
                  className="collapse"
                  aria-labelledby="headingTen"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Fundamental que de aviso a la organización lo antes posible
                    para poder tener controlados a todos los participantes y
                    velar por su seguridad.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingEleven">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseEleven"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      ¿Cuándo puedo retirar el dorsal?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseEleven"
                  className="collapse"
                  aria-labelledby="headingEleven"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Se establecerá con tiempo suficiente el lugar y horas de
                    entrega de dorsales.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="headingTwelve">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapseTwelve"
                      aria-expanded="false"
                      aria-controls="collapseTwelve"
                    >
                      ¿Puede retirar el dorsal algún compañero por mí?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseTwelve"
                  className="collapse"
                  aria-labelledby="headingTwelve"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Sí, siempre que tenga DNI y Licencia de la federación, y los
                    papeles que se deben tener cubiertos para la prueba.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="heading13">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapse13"
                      aria-expanded="false"
                      aria-controls="collapse13"
                    >
                      ¿Tengo que pagar el seguro federativo?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapse13"
                  className="collapse"
                  aria-labelledby="heading13"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    SOLO si no tienes la licencia Federativa en Vigor. La
                    federación tiene la opción de LICENCIA DE DÍA para el
                    evento, que en el caso de la Costa Atlantica MTB Tour es de
                    30 euros por las 3 etapas.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="heading14">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapse14"
                      aria-expanded="false"
                      aria-controls="collapse14"
                    >
                      ¿Qué hay en los avituallamientos?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapse14"
                  className="collapse"
                  aria-labelledby="heading14"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    En los avituallamientos se dispondrá de: Avituallamiento
                    sólido/líquido ( para los participantes): fruta variada,
                    frutos secos, agua, bebida isotónica, coca cola, pastelitos,
                    etc.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="heading15">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapse15"
                      aria-expanded="false"
                      aria-controls="collapse15"
                    >
                      ¿Es obligatorio recoger el trofeo si lo gano?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapse15"
                  className="collapse"
                  aria-labelledby="heading15"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    SI. Es obligatorio. En cada una de las etapas, los líderes
                    de la general y de cada categoría, recibirán el maillot de
                    líder y SERÁ OBLIGATORIO, que corran con dicho maillot en la
                    siguiente etapa. Los ganadores finales también deben estar
                    en el pódium para la recogida de premios el último dia de la
                    prueba.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="heading16">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapse16"
                      aria-expanded="false"
                      aria-controls="collapse16"
                    >
                      ¿Hace falta dispositivo GPS?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapse16"
                  className="collapse"
                  aria-labelledby="heading16"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    Si, es preciso en la prueba, aunque si se corre en parejas o
                    se decide ir varias personas juntas, con que una de ellas
                    tenga dicho dispositivo, será suficiente. La prueba tiene
                    puntos de marcaje reforzado para facilitar el guiado.
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header" id="heading17">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      data-toggle="collapse"
                      data-target="#collapse17"
                      aria-expanded="false"
                      aria-controls="collapse17"
                    >
                      ¿Si no voy se devuelve la cuota que he pagado?
                    </button>
                  </h5>
                </div>
                <div
                  id="collapse17"
                  className="collapse"
                  aria-labelledby="heading17"
                  data-parent="#accordion"
                >
                  <div className="card-body">
                    No hay devolución económica salvo en las fechas y
                    porcentajes marcados por la organización en el reglamento.
                    EN CASO DE QUE EL COVID impida celebrar la prueba, los
                    participantes podrán decidir participar en la nueva fecha, o
                    solicitar en soporte@emesports.es la devolución de su tarifa
                    ( menos 2 euros de gastos de gestión
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </Page>
);

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
});

export default connect(
  mapStateToProps,
  null
)(Rates);
