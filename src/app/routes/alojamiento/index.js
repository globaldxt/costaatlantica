import React from "react";
import Page from "../../components/page";
import alda from "../../assets/alda.png";

import "./alojamiento.css";

export default () => (
  <Page id="alojamiento" title="Alojamiento" noCrawl>
    <div className="row">
      <div className="col-12">
        <h4 className="paquetes-title">Alojamiento</h4>
        <div className="row py-5">
          <div className="col-8 offset-2">
            <article>
              {/* <h4>
                <FormattedMessage id="Hosting.title" defaultMessage="Hotel Alda Pontevedra" />
              </h4> */}
              <img src={alda} width="300" alt="hotel" />
              <p className="my-4">
                Información:{" "}
                <a
                  className="Hosting-web"
                  href="https://www.aldahotels.es/alojamientos/hotel-alda-estacion-pontevedra/"
                >
                  Hotel Alda Pontevedra
                </a>
              </p>
            </article>
          </div>
        </div>
      </div>
    </div>
  </Page>
);
