import React from "react";
import { Link } from "react-router-dom";
import Media from "react-media";

import Page from "../../components/page";
import Countdown from "../../components/Countdown";
import Slider from "../../components/Slider";
// import ModalExample from "../../components/ModalExample";
import Arrow from "../../assets/rightArrow.svg";
import heroImage from "../../assets/hero.png";

import "./homepage.css";

export default () => (
  <Page id="homepage">
    <Slider />
    <section className="Home-countdown">
      <Media query={{ minWidth: 768 }}>
        {matches => (matches ? "" : <Countdown date="2021-09-25T00:00:00" />)}
      </Media>
    </section>
    {/* <section className="hero">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 col-lg-6">
            <img className="img-fluid" src={heroImage} alt="image" />
          </div>
          <div className="col-12 col-lg-6">
            <div className="hero-content">
              <h1 className="hero-content-title">Costa Atlántica MTB Tour</h1>
              <h5>24, 25 y 26 de septiembre</h5>
              <p className="hero-content-subtitle">
                Inscripciones abiertas a partir del 2 de abril de 2021
              </p>
              <Countdown date="2021-09-25T00:00:00" />
              <button className="btn btn-primary btn-inscription">Inscríbete</button>
              <h2 className="gps-guided">GPS GUIDED</h2>
            </div>
          </div>
        </div>
      </div>
    </section> */}
    <section className="Home-pager">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12 col-lg-4">
            <Link className="Home-pager-link" to="">
              <h2 className="Home-pager-title">Prueba en directo</h2>
              <h5 className="Home-pager-subtitle">
                Sigue al minuto la prueba través de las redes sociales
              </h5>
              <span className="Home-pager-arrow-wrapper">
                <img className="Home-pager-arrow" src={Arrow} alt="arrow" />
              </span>
            </Link>
          </div>
          <div className="col-md-12 col-lg-4">
            <a
              className="Home-pager-link"
              href="https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/participantes/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <h2 className="Home-pager-title">Inscritos</h2>
              <h5 className="Home-pager-subtitle">
                Consulta la lista de inscritos
              </h5>
              <span className="Home-pager-arrow-wrapper">
                <img className="Home-pager-arrow" src={Arrow} alt="arrow" />
              </span>
            </a>
          </div>
          <div className="col-md-12 col-lg-4">
            <Link className="Home-pager-link" to="">
              <h2 className="Home-pager-title">Resultados de la prueba</h2>
              <h5 className="Home-pager-subtitle">
                Comprueba tu posición de esta edición
              </h5>
              <span className="Home-pager-arrow-wrapper">
                <img className="Home-pager-arrow" src={Arrow} alt="arrow" />
              </span>
            </Link>
          </div>
        </div>
      </div>
    </section>
    <div className="container">
      <div className="row">
        <div className="col-12">
          <section className="description">
            <h4 className="description-title">Costa Atlántica MTB Tour</h4>
            <h6 className="description-text">
              La <strong>Costa Atlántica MTB Tour</strong>, es una competición
              por etapas de bicicleta de montaña. los participantes disfrutarán
              de 3 etapas: una crono nocturna y dos etapas maratón. La prueba
              tiene un <strong> formato CHALLENGE</strong>, lo que quiere decir
              que cada etapa se disputa de forma independiente, por lo que
              aunque un participante/pareja no finalice alguna de las etapas,
              podrán disputar el resto. Si un participante/pareja, no finaliza
              una etapa,{" "}
              <strong>
                <u>
                  en la general se computará en dicha etapa, el tiempo del
                  último participante/pareja.
                </u>
              </strong>
              <br />
              <br />
              Los participantes se enfrentarán tanto de forma individual como
              por parejas en cualquiera de las dos modalidades. La prueba
              comienza el viernes 24 de septiembre de 2020 a las 20:00 con una
              CRONO NOCTURNA en Pontevedra (en esta prueba, se decidirán los
              cajones del día siguiente,), primera etapa se desarrolla el sábado
              25, con salida a las 9:30 de la mañana desde la localidad de
              Pontevedra, la segunda etapa tiene salida el domingo 26 en la
              localidad a determinar a las 9:30 horas, dando por finalizada la
              prueba.
              <br />
              <br />
              La competición se disputa según el reglamento técnico y deportivo
              de la Real Federación Española de Ciclismo (RFEC).
            </h6>
          </section>
        </div>
      </div>
    </div>
    <section className="video">
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-8 offset-md-2">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe
                width="560"
                height="315"
                src="https://www.youtube.com/embed/px4Nk9d1_dA"
                frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              />
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* <section className="image-grid">
      <div className="row no-gutters">
        <div className="col-12 col-sm-4">
          <div className="img-wrapper">
            <img className="img-fluid" src={home1} alt="image" />
          </div>
        </div>
        <div className="col-12 col-sm-4">
          <div className="img-wrapper">
            <img className="img-fluid" src={home2} alt="image" />
          </div>
        </div>
        <div className="col-12 col-sm-4">
          <div className="img-wrapper">
            <img className="img-fluid" src={home3} alt="image" />
          </div>
        </div>
      </div>
    </section> */}
    {/* <ModalExample /> */}
  </Page>
);
