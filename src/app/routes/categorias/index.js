import React from "react";
import Page from "../../components/page";

import "./categorias.css";

export default () => (
  <Page id="categorias" title="Categorías">
    <div className="row">
      <div className="col-12">
        <h4 className="categorias-title">Categorías</h4>
        <p className="categorias-subtitle text-center">
          Los participantes se pueden inscribir en equipos masculinos, femeninos
          o mixtos. Las categorías disponiles son las siguientes
        </p>
      </div>
    </div>
    <div className="row mb-5">
      <div className="col-12 col-md-4 categoria-type p-5">
        <h5>
          Categoría
          <br /> Élite
        </h5>
      </div>
      <div className="col-12 col-md-8 categoria-description p-5">
        <ul>
          <li>
            <strong>Elite masculino:</strong> Una pareja compuesta por dos
            hombres que debe tener 19 años o más el 31 de diciembre del año en
            que se celebra la competición. Debe poseer una licencia Elite
            homologada.
          </li>
          <li>
            <strong>Elite femenina:</strong> una pareja compuesta por dos
            mujeres que debe tener 19 años o más el 31 de diciembre del año en
            que se celebra la competición. Debe poseer una licencia Elite
            homologada.
          </li>
        </ul>
      </div>
    </div>
    <div className="row mb-5">
      <div className="col-12 col-md-8 categoria-description p-5 order-2 order-md-1">
        <ul>
          <li>
            <strong>Master 30:</strong> Una pareja compuesta por dos hombre que
            debe tener 30 años o más el 31 de diciembre del año en que se
            celebra la competición. Debe poseer una licencia Master homologada
            para ciclismo de competición. Quienes posean licencia de categoría
            Elite no podrán competir en esta categoría.
          </li>
          <li>
            <strong>Master 40:</strong> Una pareja compuesta por dos hombres que
            debe tener 40 años o más el 31 de diciembre del año en que se
            celebra la competición. Debe poseer una licencia Master homologada
            para ciclismo de competición. Quienes posean licencia de categoría
            Elite o Master 30 no podrán competir en esta categoría.
          </li>
          <li>
            <strong>Master 50:</strong> Una pareja compuesta por dos hombres que
            debe tener 50 años o más el 31 de diciembre del año en que se
            celebra la competición. Debe poseer una licencia Master homologada
            para ciclismo de competición. Quienes posean licencia de categoría
            Elite, Master 30 o Master 40 no podrán competir en esta categoría.
          </li>
          <li>
            <strong>Master femenina:</strong> Una pareja compuesta por dos
            mujeres que debe tener 30 años o más el 31 de diciembre del año en
            que se celebra la competición. Debe poseer una licencia Master
            homologada para ciclismo de competición. Quienes posean licencia de
            categoría Elite no podrán competir en esta categoría.
          </li>
        </ul>
      </div>
      <div className="col-12 col-md-4 categoria-type p-5 order-1 order-md-2">
        <h5>
          Categoría
          <br /> Master
        </h5>
      </div>
    </div>
    <div className="row mb-5">
      <div className="col-12 col-md-4 categoria-type p-5">
        <h5>
          Categoría
          <br /> Open
        </h5>
      </div>
      <div className="col-12 col-md-8 categoria-description p-5">
        <ul>
          <li>
            <strong>Categoría OPEN PAREJAS MASCULINO:</strong> esta categoría
            son todos los hombres que deben tener un mínimo de 18 años . Aquí
            participarán aquellas parejas en las que al menos uno de sus
            miembros tenga Licencia cicloturista, o Licencia de Día o edades
            diferentes para poder entrar en la categoría master ( ejemplo una
            persona de 40 años y una de 30 años)
          </li>
          <li>
            <strong>Categoría OPEN PAREJAS FEMENINO:</strong> esta categoría son
            todas las mujeres que deben tener un mínimo de 18 años . Aquí
            participarán aquellas parejas en las que al menos uno de sus
            miembros tenga Licencia cicloturista, o Licencia de Día.
          </li>
          <li>
            <strong>Categoría OPEN INDIVIDUAL MASCULINO:</strong> esta categoría
            son todos los hombres que deben tener un mínimo de 18 años y que
            deseen competir de forma individual. Los participantes que no posean
            licencia federativa y deseen adquirir una licencia de día, deberán
            participar en esta categoría, asi como los que posean licencia
            cicloturista.
          </li>
          <li>
            <strong>Categoría OPEN INDIVIDUAL FEMENINO:</strong> esta categoría
            son todas las mujeres que deben tener un mínimo de 18 años y que
            deseen competir de forma individual. Los participantes que no posean
            licencia federativa y deseen adquirir una licencia de día, deberán
            participar en esta categoría. Así como las que posean licencia
            cicloturista.
          </li>
        </ul>
      </div>
    </div>
    <div className="row mb-5">
      <div className="col-12 col-md-8 categoria-description p-5 order-2 order-md-1">
        <ul>
          <li>
            <strong>Mixta:</strong> En esta categoría participarán 1 hombre y
            una mujer con un mínimo de 18 años cumplidos a 31 de diciembre del
            año en que se celebra la competición.
          </li>
        </ul>
      </div>
      <div className="col-12 col-md-4 categoria-type p-5 order-1 order-md-2">
        <h5>
          Categoría
          <br /> Mixta
        </h5>
      </div>
    </div>
  </Page>
);
