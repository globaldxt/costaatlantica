import React from "react";
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";

import NotFound from "./not-found";

const Homepage = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ "./homepage"),
  loading: () => null,
  modules: ["homepage"]
});

const Stages = Loadable({
  loader: () => import(/* webpackChunkName: "about" */ "./etapas"),
  loading: () => null,
  modules: ["etapas"]
});

const Rates = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ "./tarifas"),
  loading: () => null,
  modules: ["tarifas"]
});

const Alojamiento = Loadable({
  loader: () => import(/* webpackChunkName: "login" */ "./alojamiento"),
  loading: () => null,
  modules: ["alojamiento"]
});

const Rules = Loadable({
  loader: () => import(/* webpackChunkName: "logout" */ "./reglamento"),
  loading: () => null,
  modules: ["reglamento"]
});

const Multimedia = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./multimedia"),
  loading: () => null,
  modules: ["multimedia"]
});

const Categories = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./categorias"),
  loading: () => null,
  modules: ["categorias"]
});

const Participantes = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./participantes"),
  loading: () => null,
  modules: ["participantes"]
});

const GlobalRaces = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./pruebas-global"),
  loading: () => null,
  modules: ["pruebas-global"]
});

const FriendsRaces = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./pruebas-amigas"),
  loading: () => null,
  modules: ["pruebas-amigas"]
});

const Wear = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./ropa-evento"),
  loading: () => null,
  modules: ["ropa-evento"]
});

const Ambassadors = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./embajadores"),
  loading: () => null,
  modules: ["embajadores"]
});

const Contact = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./contacto"),
  loading: () => null,
  modules: ["contacto"]
});

const Paquetes = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./paquetes"),
  loading: () => null,
  modules: ["paquetes"]
});

export default () => (
  <Switch>
    <Route exact path="/" component={Homepage} />
    <Route exact path="/etapas" component={Stages} />
    <Route exact path="/tarifas" component={Rates} />
    <Route exact path="/alojamiento" component={Alojamiento} />
    <Route exact path="/reglamento" component={Rules} />
    <Route exact path="/categorias" component={Categories} />
    <Route exact path="/multimedia" component={Multimedia} />
    <Route exact path="/participantes" component={Participantes} />
    <Route exact path="/embajadores" component={Ambassadors} />
    <Route exact path="/ropa-evento" component={Wear} />
    <Route exact path="/pruebas-global" component={GlobalRaces} />
    <Route exact path="/pruebas-amigos" component={FriendsRaces} />
    <Route exact path="/paquetes" component={Paquetes} />
    <Route exact path="/contacto" component={Contact} />

    <Route component={NotFound} />
  </Switch>
);
