import React from "react";
import { connect } from "react-redux";
import Page from "../../components/page";

const Rates = ({ currentUser }) => (
  <Page id="dashboard" title="Dashboard" noCrawl>
    <h6 className="title">Pruebas GlobalDXT</h6>
  </Page>
);

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
});

export default connect(
  mapStateToProps,
  null
)(Rates);
