import React from "react";
import { connect } from "react-redux";
import Page from "../../components/page";
import "./reglamento.css";
import reglamento from "../../assets/reglamento.pdf";

const Rates = ({ currentUser }) => (
  <Page id="reglamento" title="Reglamento" noCrawl>
    <div className="container">
      <div className="row">
        <div className="col-12">
          <section className="reglamento text-center">
            <h4 className="reglamento-title">Reglamento</h4>
            <p>
              La COSTA ATLÁNTICA MTB TOUR, es una competición por etapas de
              bicicleta de montaña, dividida en tres etapas maratón en la que
              los participantes se enfrentarán tanto de forma individual como
              por parejas.
            </p>
            <p>
              La competición se disputa según el reglamento técnico y deportivo
              de la Real Federación Española de Ciclismo (RFEC).
            </p>
            <p>Puedes descargar el reglamento completo desde este enlace</p>
            <a
              className="btn btn-primary"
              href={reglamento}
              target="_blank"
              rel="noopener noreferrer"
            >
              Reglamento
            </a>
          </section>
        </div>
      </div>
    </div>
  </Page>
);

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
});

export default connect(
  mapStateToProps,
  null
)(Rates);
