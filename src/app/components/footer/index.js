import React from "react";

import "./footer.css";

import image1 from "../../assets/image1.jpg";
import image2 from "../../assets/image2.jpg";
import image3 from "../../assets/image3.jpg";

export default () => (
  <footer id="footer" className="footer">
    <div className="row no-gutters">
      <div className="col-12 col-md-4">
        <div className="footer-image-wrapper">
          <img className="img-fluid" src={image1} alt="image" />
        </div>
      </div>
      <div className="col-12 col-md-4">
        <div className="footer-image-wrapper">
          <img className="img-fluid" src={image2} alt="image" />
        </div>
      </div>
      <div className="col-12 col-md-4">
        <div className="footer-image-wrapper">
          <img className="img-fluid" src={image3} alt="image" />
        </div>
      </div>
    </div>
    <div className="row no-gutters">
      <div className="col-12">
        <p className="copyright">2019. GlobalDXT S.L.</p>
      </div>
    </div>
  </footer>
);
