import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";

const ModalExample = props => {
  const { buttonLabel, className } = props;

  const [modal, setModal] = useState(true);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>
        {buttonLabel}
      </Button>
      <Modal size="lg" isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>
          <strong>
            COMUNICADO OFICIAL ORGANIZACIÓN <br />
            APLAZAMIENTO COSTA ATLANTICA MTB TOUR 2021.
          </strong>
        </ModalHeader>
        <ModalBody>
          <p>
            Después de intentar que la actual situación que estamos viviendo no
            afectara a la prueba, y de buscar todas las opciones posibles,
            <strong>
              sentimos deciros que la actual edición de la COSTA ATLÁNTICA MTB
              TOUR, se traslada al 23-24-25-26 de septiembre de 2021.
            </strong>
          </p>
          <p>
            La complicación logística del evento y las necesidades de
            colaboración de múltiples administraciones tanto autonómicas como
            locales, hace que no sea posible realizar evento este año. Algunos
            de los ayuntamientos que son sede del evento, tienen volcados todos
            sus esfuerzos en la actual crisis sanitaria, lo que les hace
            imposible poder albergar la prueba este año, y esto afecta
            directamente a las posibilidades de realización de la misma.
          </p>
          <p>
            Han sido muchas horas de trabajo y dedicación, de búsqueda de
            patrocinios, preparación de todos los aspectos logísticos, revisión
            de trazados, etc; pero la realidad nos obliga a trasladar el evento
            a 2021.
          </p>
          <p>
            Queremos dar las gracias a todos los que han colaborado de una u
            otra forma en esta fiesta del MTB, esperando que sigan confiando en
            la prueba para el siguiente año.{" "}
          </p>
          <p>
            Seguro que 2021 nos depara una COSTA ATLANTICA MTB llena de
            sorpresas.
          </p>
          <p>
            Informar a los participantes que a lo largo de los próximos días le
            será reingresada el 100 % su inscripción que será devuelto por el
            mismo medio por el que se ha efectuado el pago.
          </p>
          <p>
            Os animamos a todos a participar en el resto de las pruebas que
            tiene GlobalDXT este año, para que así podamos seguir disfrutando de
            nuestra pasión.
          </p>
          <p>
            Para cualquier consulta al respecto podeis poneros en contacto en el
            mail globaldxt@gmail.com
          </p>
          <p>Un fuerte abrazo de toda la organización.</p>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ModalExample;
