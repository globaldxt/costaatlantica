import React from "react";
import { Link } from "react-router-dom";

import logo from "../../assets/logo.png";

import "./header.css";

const links = [
  {
    to: "/etapas",
    text: "Etapas"
  },
  {
    to: "/categorias",
    text: "Categorías"
  },
  {
    to: "/tarifas",
    text: "Tarifas"
  },
  {
    to: "/alojamiento",
    text: "Alojamiento"
  },
  {
    to: "/paquetes",
    text: "Paquetes"
  },
  {
    to: "/reglamento",
    text: "Reglamento"
  },
  {
    to: "/participantes",
    text: "Participantes"
  }
  // {
  //   to: "https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/inscripcion_datos/",
  //   text: "Inscripción"
  // }
  // {
  //   to: "/multimedia",
  //   text: "Multimedia"
  // },
  // {
  //   to: "/pruebas-global",
  //   text: "Pruebas GlobalDXT"
  // },
  // {
  //   to: "/pruebas-amigas",
  //   text: "Pruebas amigas"
  // },
  // {
  //   to: "/ropa-evento",
  //   text: "Ropa evento"
  // },
  // {
  //   to: "/embajadores",
  //   text: "Embajadores"
  // }
];

const isCurrent = (to, current) => {
  if (to === "/" && current === to) {
    return true;
  } else if (to !== "/" && current.includes(to)) {
    return true;
  }

  return false;
};

const HeaderLink = ({ to, text, current }) => (
  <li className={isCurrent(to, current) ? "current nav-item" : "nav-item"}>
    <Link to={to}>{text}</Link>
  </li>
);

export default ({ isAuthenticated, current }) => (
  <header id="header" className="header">
    <div className="logo-wrapper">
      <a href="/">
        <img className="logo" src={logo} alt="logo" />
      </a>
    </div>
    <nav className="nav">
      {links.map((link, index) => {
        const TheLink = <HeaderLink key={index} current={current} {...link} />;

        if (link.hasOwnProperty("auth")) {
          if (link.auth && isAuthenticated) {
            return TheLink;
          } else if (!link.auth && !isAuthenticated) {
            return TheLink;
          }

          return null;
        }

        return TheLink;
      })}
      <li className="nav-item">
        <a
          target="_blank"
          href="https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/inscripcion_datos/"
        >
          Inscripción{" "}
        </a>
      </li>
    </nav>
  </header>
);
