import React, { Component } from "react";
import { Link } from "react-router-dom";
import Close from "./x.svg";

class Menu extends Component {
  render() {
    var visibility = "hide";

    if (this.props.menuVisibility) {
      visibility = "show";
    }

    return (
      <div id="flyoutMenu" className={visibility}>
        <button className="btn" onMouseDown={this.props.handleMouseDown}>
          <img src={Close} alt="icon" />
        </button>
        <ul>
          <h3>
            <Link className="Header-link" to="/etapas">
              Etapas
            </Link>
          </h3>
          <h3>
            <Link className="Header-link" to="/categorias">
              Categorías
            </Link>
          </h3>
          <h3>
            <Link className="Header-link" to="/tarifas">
              Tarifas
            </Link>
          </h3>
          <h3>
            <Link className="Header-link" to="/alojamiento">
              Alojamiento
            </Link>
          </h3>
          <h3>
            <Link className="Header-link" to="/paquetes">
              Paquetes
            </Link>
          </h3>
          <h3>
            <Link className="Header-link" to="/reglamento">
              Reglamento
            </Link>
          </h3>
          <h3>
            <Link className="Header-link" to="/participantes">
              Participantes
            </Link>
          </h3>
          <h3>
            <a
              className="Header-link"
              href="https://eventos.emesports.es/inscripcion/costa-atlantica-mtb-tour-2021/inscripcion_datos/"
            >
              Inscripción
            </a>
          </h3>
        </ul>
      </div>
    );
  }
}

export default Menu;
