import React from "react";
import "./HeaderMobile.css";
import logo from "../../assets/logo.png";
import Menu from "./Menu";
import MenuButton from "./MenuButton";
import { connect } from "react-redux";

class HeaderMobile extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      visible: false
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  handleMouseDown(e) {
    this.toggleMenu();

    e.stopPropagation();
  }

  toggleMenu() {
    this.setState({
      visible: !this.state.visible
    });

    console.log(this.state);
  }

  render() {
    return (
      <header className="header-mobile">
        <div className="header-wrapper">
          <div className="logo-wrapper">
            <a href="/">
              <img className="logo" src={logo} alt="logo" />
            </a>
          </div>
          <MenuButton handleMouseDown={this.handleMouseDown} />
          <Menu
            handleMouseDown={this.handleMouseDown}
            menuVisibility={this.state.visible}
          />
        </div>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  visible: state.visible
});

export default connect(
  mapStateToProps,
  null
)(HeaderMobile);
