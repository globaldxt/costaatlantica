import React from "react";
import Ham from "./menu.svg";

class MenuButton extends React.Component {
  render() {
    return (
      <button className="btn btn-ham" onMouseDown={this.props.handleMouseDown}>
        <img className="ham-mobile" src={Ham} alt="icon" />
      </button>
    );
  }
}

export default MenuButton;
