// prefer default export if available
const preferDefault = m => (m && m.default) || m;

exports.components = {
  "component---cache-dev-404-page-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/.cache/dev-404-page.js")
  ),
  "component---src-pages-404-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/404.js")
  ),
  "component---src-pages-alojamiento-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/alojamiento.js")
  ),
  "component---src-pages-categorias-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/categorias.js")
  ),
  "component---src-pages-etapas-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/etapas.js")
  ),
  "component---src-pages-index-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/index.js")
  ),
  "component---src-pages-paquetes-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/paquetes.js")
  ),
  "component---src-pages-participantes-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/participantes.js")
  ),
  "component---src-pages-reglamento-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/reglamento.js")
  ),
  "component---src-pages-tarifas-js": preferDefault(
    require("/home/jacoboar/Proyectos/globaldxt/costaatlantica/src/pages/tarifas.js")
  )
};
