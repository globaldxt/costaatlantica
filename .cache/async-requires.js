// prefer default export if available
const preferDefault = m => (m && m.default) || m;

exports.components = {
  "component---cache-dev-404-page-js": () =>
    import("./../../dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-js": () =>
    import("./../../../src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-alojamiento-js": () =>
    import("./../../../src/pages/alojamiento.js" /* webpackChunkName: "component---src-pages-alojamiento-js" */),
  "component---src-pages-categorias-js": () =>
    import("./../../../src/pages/categorias.js" /* webpackChunkName: "component---src-pages-categorias-js" */),
  "component---src-pages-etapas-js": () =>
    import("./../../../src/pages/etapas.js" /* webpackChunkName: "component---src-pages-etapas-js" */),
  "component---src-pages-index-js": () =>
    import("./../../../src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */),
  "component---src-pages-paquetes-js": () =>
    import("./../../../src/pages/paquetes.js" /* webpackChunkName: "component---src-pages-paquetes-js" */),
  "component---src-pages-participantes-js": () =>
    import("./../../../src/pages/participantes.js" /* webpackChunkName: "component---src-pages-participantes-js" */),
  "component---src-pages-reglamento-js": () =>
    import("./../../../src/pages/reglamento.js" /* webpackChunkName: "component---src-pages-reglamento-js" */),
  "component---src-pages-tarifas-js": () =>
    import("./../../../src/pages/tarifas.js" /* webpackChunkName: "component---src-pages-tarifas-js" */)
};
